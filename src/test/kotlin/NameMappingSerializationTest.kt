import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import models.DateThresholdValue
import models.NameMapping
import java.time.OffsetDateTime
import java.time.ZoneOffset
import kotlin.test.Test
import kotlin.test.assertEquals

class NameMappingSerializationTest {
    private val earliestDate = OffsetDateTime.of(2010, 1, 1, 0, 0, 0, 0, ZoneOffset.of("-04:00"))
    private val mappings = listOf(
        NameMapping(
            originalFieldValue = "Terry Smith",
            newFieldValues = listOf(
                DateThresholdValue(
                    earliestDate.plusYears(1),
                    newValue = "Terry Smith"
                ),
                DateThresholdValue(
                    earliestDate.plusYears(2),
                    newValue = "Company 1"
                ),
                DateThresholdValue(
                    earliestDate.plusYears(5),
                    newValue = "Company 2"
                ),
            )
        ),
        NameMapping(
            originalFieldValue = "Aiden Fisher",
            newFieldValues = listOf(
                DateThresholdValue(
                    earliestDate.plusYears(4),
                    newValue = "Company 3"
                ),
                DateThresholdValue(
                    earliestDate.plusYears(1),
                    newValue = "Company 1"
                ),
                DateThresholdValue(
                    earliestDate.plusYears(2),
                    newValue = "Company 2"
                ),
            )
        ),
        NameMapping(
            originalFieldValue = "Pat Miller",
            newFieldValues = listOf(
                DateThresholdValue(
                    earliestDate.plusYears(1000),
                    newValue = "Pat Miller"
                ),
            )
        ),
    )

    @Test
    fun testRoundTripSerialization() {
        val json = Json { prettyPrint = true }.encodeToString(mappings)
        val fromJson = Json {  }.decodeFromString<List<NameMapping>>(json)
        println(json)
        assertEquals(mappings, fromJson)
    }
}