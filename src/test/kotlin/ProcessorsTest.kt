import com.github.michaelbull.result.get
import models.HeaderData
import processors.*
import java.nio.charset.Charset
import java.nio.file.Paths
import kotlin.io.path.ExperimentalPathApi
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ProcessorsTest {
    private val directory = Paths.get("/tmp/twitter4kt").toFile()

    @Test
    fun testGitShowProcessor() {
        println(GitShowProcessor(directory).process(HeaderData(gitHash = "bed8a1c93263b0fac8f863eb2a5851659504ba27")))
        println(GitShowProcessor(directory).process(HeaderData(gitHash = "a888e247a435ce6f0dd725d1bea4eab458f09e98")))
    }

    @Test
    fun testGetLogDataProcessors() {
        //GitLogProcessor(directory).process().forEach { println(it) }
        GitLogProcessor(directory, "9e1ebe9655e2220448936b8911fc77b07ffd9f0b").process().forEach { println(it) }
    }

    @Test
    fun testGitLsFilesProcessor() {
        GitLsFilesProcessor(directory).process().forEach { println("${it.fileName} -> ${it.toFile().extension}") }
    }

    @Test
    fun testGitCheckoutProcessor() {
        println(GitCheckoutProcessor(directory).checkout("master"))
    }

    @Test
    fun testFileSetTotalLinesProcessor() {
        val files = GitLsFilesProcessor(directory).process()
        println(FileSetTotalLinesProcessor(directory).process(files))
    }

    @ExperimentalPathApi
    @Test
    fun testGitHistoryWalkerProcessor() {
        GitHistoryWalkProcessor(directory, listOf(), listOf("jar"), false, listOf()).process("master").get()?.forEach { println(it) }
    }

    @Test
    fun testGitSimpleHistoryWalkerProcessor() {
        GitSimpleHistoryWalkProcessor(directory, listOf()).process("master").get()?.forEach { println(it) }
    }

    @Test
    fun encodingsList() {
        Charset.availableCharsets().forEach{ println(it.key) }
    }

    @ExperimentalPathApi
    @Test
    fun pathTest() {
        val path1 = "testgit/.hidden/test1.txt"
        val path2 = "testgit/folder/.hiddenfile1.txt"
        val path3 = Paths.get("/home/user1/test", "./newpath/file.txt")
        assertTrue(Paths.get(path1).pathIsHidden())
        assertTrue(Paths.get(path2).pathIsHidden())
        assertFalse(path3.pathIsHidden())
    }
}

