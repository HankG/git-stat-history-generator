import models.DateThresholdValue
import models.NameMapping
import normalizers.NameMappingNormalizer
import serialization.OpenJdkPeopleCsvReader
import java.lang.IllegalArgumentException
import java.nio.file.Paths
import java.time.OffsetDateTime
import java.time.ZoneOffset
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.Ignore

class NameMappingNormalizerTest {
    private val earliestDate = OffsetDateTime.of(2010, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC)
    private val mapper = NameMappingNormalizer(
        useDefaultIfNotFound = true,
        nameMapping = listOf(
            NameMapping(
                originalFieldValue = "Terry Smith",
                newFieldValues = listOf(
                    DateThresholdValue(
                        earliestDate.plusYears(1),
                        newValue = "Terry Smith"
                    ),
                    DateThresholdValue(
                        earliestDate.plusYears(2),
                        newValue = "Company 1"
                    ),
                    DateThresholdValue(
                        earliestDate.plusYears(5),
                        newValue = "Company 2"
                    ),
                )
            ),
            NameMapping(
                originalFieldValue = "Aiden Fisher",
                newFieldValues = listOf(
                    DateThresholdValue(
                        earliestDate.plusYears(4),
                        newValue = "Company 3"
                    ),
                    DateThresholdValue(
                        earliestDate.plusYears(1),
                        newValue = "Company 1"
                    ),
                    DateThresholdValue(
                        earliestDate.plusYears(2),
                        newValue = "Company 2"
                    ),
                )
            ),
            NameMapping(
                originalFieldValue = "Pat Miller",
                newFieldValues = listOf(
                    DateThresholdValue(
                        earliestDate.plusYears(1000),
                        newValue = "Pat Miller"
                    ),
                )
            ),
        )
    )


    @Test
    fun testWithEmptyMapping() {
        val mapper = NameMappingNormalizer(listOf(), true)
        assertEquals(mapper.defaultValueIfNotFound, mapper.transform("some name", OffsetDateTime.now()))
        assertFailsWith<IllegalArgumentException> {
            NameMappingNormalizer(listOf(), false).transform("some name", OffsetDateTime.now())
        }
    }

    @Test
    fun testWithUnknownValue() {
        assertEquals(mapper.defaultValueIfNotFound, mapper.transform("some name", OffsetDateTime.now()))
    }

    @Test
    fun testWithKnownValueWithinTimes() {
        assertEquals("Company 1", mapper.transform("Terry Smith", earliestDate.plusYears(3)))
    }

    @Test
    fun testWithKnownValueButNotInTimes() {
        assertEquals(mapper.defaultValueIfNotFound, mapper.transform("Terry Smith", earliestDate))
    }

    @Test
    fun testKnownValueTimeMarching() {
        assertEquals(mapper.defaultValueIfNotFound, mapper.transform("Terry Smith", earliestDate))
        assertEquals("Terry Smith", mapper.transform("Terry Smith", earliestDate.plusYears(1)))
        assertEquals("Terry Smith", mapper.transform("Terry Smith", earliestDate.plusMonths(18)))
        assertEquals("Company 1", mapper.transform("Terry Smith", earliestDate.plusYears(2)))
        assertEquals("Company 1", mapper.transform("Terry Smith", earliestDate.plusYears(3)))
        assertEquals("Company 1", mapper.transform("Terry Smith", earliestDate.plusYears(4)))
        assertEquals("Company 2", mapper.transform("Terry Smith", earliestDate.plusYears(5)))
        assertEquals("Company 2", mapper.transform("Terry Smith", earliestDate.plusYears(6)))
    }

    @Test
    fun testTimeMarchingWithRawOutOfOrderData() {
        assertEquals(mapper.defaultValueIfNotFound, mapper.transform("Aiden Fisher", earliestDate))
        assertEquals(mapper.defaultValueIfNotFound, mapper.transform("Aiden Fisher", earliestDate.plusMonths(6)))
        assertEquals("Company 1", mapper.transform("Aiden Fisher", earliestDate.plusMonths(12)))
        assertEquals("Company 1", mapper.transform("Aiden Fisher", earliestDate.plusMonths(18)))
        assertEquals("Company 2", mapper.transform("Aiden Fisher", earliestDate.plusYears(2)))
        assertEquals("Company 2", mapper.transform("Aiden Fisher", earliestDate.plusYears(3)))
        assertEquals("Company 3", mapper.transform("Aiden Fisher", earliestDate.plusYears(4)))
        assertEquals("Company 3", mapper.transform("Aiden Fisher", earliestDate.plusYears(5)))
    }

    @Test
    @Ignore
    fun testFromCsvFile() {
        val path = Paths.get("/media/sf_VmShare/OpenJdkPeople.csv").toFile()
        val mappings = OpenJdkPeopleCsvReader.read(path, true, OpenJdkPeopleCsvReader.NameKey.RealName)
        val mapper = NameMappingNormalizer(mappings, true)
        println(mapper.transform("Bino George", OffsetDateTime.now()))
        println(mapper.transform("Boy George", OffsetDateTime.now()))
    }
}