import models.HeaderData
import java.lang.IllegalArgumentException
import java.time.OffsetDateTime
import kotlin.test.*

class HeaderParsersTest {

    @Test
    fun testIsHeader() {
        assertFalse("not a header".isHeaderLine())
        assertFalse("new_commit".isHeaderLine())
        assertTrue("new_commit:".isHeaderLine())
        assertTrue("new_commit:1⁓⁓⁓John Smith⁓john@mycompany.org⁓2003-05-12T16:03:04+00:00⁓".isHeaderLine())
    }

    @Test
    fun testEmptyHeader() {
        assertFailsWith(IllegalArgumentException::class) { "new_commit:".parseToHeaderData() }
    }

    @Test
    fun testGetHeaderDataFromMinimum() {
        val text = "new_commit:1⁓⁓⁓John Smith⁓john@mycompany.org⁓2003-05-12T16:03:04+00:00⁓"
        val expected = HeaderData(
            gitHash = "1",
            parentHash = "",
            authorName = "John Smith",
            authorEmail = "john@mycompany.org",
            timestamp = OffsetDateTime.parse("2003-05-12T16:03:04+00:00")
        )
        testHeader(expected, text)
    }

    @Test
    fun testGetHeaderDataWithParent() {
        val text = "new_commit:1⁓2⁓⁓John Smith⁓john@mycompany.org⁓2003-05-12T16:03:04+00:00⁓"
        val expected = HeaderData(
            gitHash = "1",
            parentHash = "2",
            authorName = "John Smith",
            authorEmail = "john@mycompany.org",
            timestamp = OffsetDateTime.parse("2003-05-12T16:03:04+00:00")
        )
        testHeader(expected, text)
    }

    @Test
    fun testGetHeaderDataWithTitle() {
        val text = "new_commit:1⁓⁓⁓John Smith⁓john@mycompany.org⁓2003-05-12T16:03:04+00:00⁓Updated checkstyle."
        val expected = HeaderData(
            gitHash = "1",
            parentHash = "",
            authorName = "John Smith",
            authorEmail = "john@mycompany.org",
            timestamp = OffsetDateTime.parse("2003-05-12T16:03:04+00:00"),
            text = "Updated checkstyle."
        )
        testHeader(expected, text)
    }

    @Test
    fun testGetWithTagData() {
        val text = "new_commit:1⁓⁓tag: tag1⁓John Smith⁓john@mycompany.org⁓2003-05-12T16:03:04+00:00⁓Updated checkstyle."
        val expected = HeaderData(
            gitHash = "1",
            parentHash = "",
            authorName = "John Smith",
            authorEmail = "john@mycompany.org",
            timestamp = OffsetDateTime.parse("2003-05-12T16:03:04+00:00"),
            text = "Updated checkstyle.",
            isTag = true
        )
        testHeader(expected, text)
    }

    @Test
    fun testFullJdkLogEntry() {
        val text = """
            new_commit:dbc9e4b50cdda35f5712deaf440c49f50b9edc96⁓b006f22f1f4312fa318f84c9e0a7cb4c928046ea⁓⁓Anton Kozlov⁓akozlov@openjdk.org⁓2021-03-25T18:10:18+00:00⁓8253795: Implementation of JEP 391: macOS/AArch64 Port 8253816: Support macOS W^X 8253817: Support macOS Aarch64 ABI in Interpreter 8253818: Support macOS Aarch64 ABI for compiled wrappers 8253819: Implement os/cpu for macOS/AArch64 8253839: Update tests and JDK code for macOS/Aarch64 8254941: Implement Serviceability Agent for macOS/AArch64 8255776: Change build system for macOS/AArch64 8262903: [macos_aarch64] Thread::current() called on detached thread,Co-authored-by: Vladimir Kempik <vkempik@openjdk.org>
            Co-authored-by: Bernhard Urban-Forster <burban@openjdk.org>
            Co-authored-by: Ludovic Henry <luhenry@openjdk.org>
            Co-authored-by: Monica Beckwith <mbeckwit@openjdk.org>
            Reviewed-by: erikj, ihse, prr, cjplummer, stefank, gziemski, aph, mbeckwit, luhenry            
        """.trimIndent()
        val expectedText = """
            8253795: Implementation of JEP 391: macOS/AArch64 Port 8253816: Support macOS W^X 8253817: Support macOS Aarch64 ABI in Interpreter 8253818: Support macOS Aarch64 ABI for compiled wrappers 8253819: Implement os/cpu for macOS/AArch64 8253839: Update tests and JDK code for macOS/Aarch64 8254941: Implement Serviceability Agent for macOS/AArch64 8255776: Change build system for macOS/AArch64 8262903: [macos_aarch64] Thread::current() called on detached thread,Co-authored-by: Vladimir Kempik <vkempik@openjdk.org>
            Co-authored-by: Bernhard Urban-Forster <burban@openjdk.org>
            Co-authored-by: Ludovic Henry <luhenry@openjdk.org>
            Co-authored-by: Monica Beckwith <mbeckwit@openjdk.org>
            Reviewed-by: erikj, ihse, prr, cjplummer, stefank, gziemski, aph, mbeckwit, luhenry
        """.trimIndent()
        val expected = HeaderData(
            gitHash = "dbc9e4b50cdda35f5712deaf440c49f50b9edc96",
            parentHash = "b006f22f1f4312fa318f84c9e0a7cb4c928046ea",
            authorName = "Anton Kozlov",
            authorEmail = "akozlov@openjdk.org",
            timestamp = OffsetDateTime.parse("2021-03-25T18:10:18+00:00"),
            text = expectedText,
            coAuthors = listOf("Bernhard Urban-Forster", "Ludovic Henry", "Monica Beckwith"),
            reviewers = listOf("erikj", "ihse", "prr", "cjplummer", "stefank", "gziemski", "aph", "mbeckwit", "luhenry")
        )
        testHeader(expected, text)
    }

    private fun testHeader(expected: HeaderData, text: String) {
        assertEquals(expected, text.parseToHeaderData())
    }
}