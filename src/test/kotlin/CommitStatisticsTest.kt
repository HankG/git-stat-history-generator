import models.CommitData
import stats.CommitStatGenerator
import stats.Grouping
import stats.StatType
import java.time.OffsetDateTime
import java.time.ZoneOffset
import kotlin.test.Test

class CommitStatisticsTest {

    @Test
    fun testFileCountHistory() {
        val initial = OffsetDateTime.of(2021, 3, 1, 0, 0, 0, 0, ZoneOffset.UTC)
        val commitData = listOf(
            CommitData(timestamp = initial, filesChanged = 10, filesAdded = 10),
            CommitData(timestamp = initial.plusDays(1), filesChanged = 3),
            CommitData(timestamp = initial.plusDays(2), filesChanged = 2, filesAdded = 1),
            CommitData(timestamp = initial.plusDays(3), filesChanged = 3, filesRemoved = 2),
            CommitData(timestamp = initial.plusDays(4), filesChanged = 6),
            CommitData(timestamp = initial.plusDays(5), filesChanged = 6, filesAdded = 2, filesRemoved = 3),
        )
        val history = CommitStatGenerator(commitData).statsOverTime(
            Grouping.Total,
            StatType.TotalFiles
        )
        history.values.first().forEach { println(it) }
    }

    @Test
    fun testLineCountHistory() {
        val initial = OffsetDateTime.of(2021, 3, 1, 0, 0, 0, 0, ZoneOffset.UTC)
        val commitData = listOf(
            CommitData(timestamp = initial, linesAdded = 10, linesRemoved = 0),
            CommitData(timestamp = initial.plusDays(1), linesAdded = 20, linesRemoved = 5),
            CommitData(timestamp = initial.plusDays(2), linesAdded = 0, linesRemoved = 2),
        )
        val history = CommitStatGenerator(commitData).statsOverTime(
            Grouping.Total,
            StatType.TotalLines
        )
        history.values.first().forEach { println(it) }
    }

}