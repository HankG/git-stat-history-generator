import models.mergeable.ActorsMergeable
import kotlin.test.Test
import kotlin.test.assertEquals

class ActorsMergeableTest {

    @Test
    fun testCombination() {
        val dev1Name = "Developer1"
        val dev2Name = "Developer2"
        val fullyLoaded = ActorsMergeable(setOf(dev1Name, dev2Name), setOf(dev1Name, dev2Name))
        val authorActor = ActorsMergeable.ofAuthor(dev1Name)
        val reviewActor = ActorsMergeable.ofReviewer(dev1Name)
        val actual = authorActor.merge(reviewActor, false)
        assertEquals(ActorsMergeable(setOf(dev1Name), setOf(dev1Name)), actual)

        val authorActor2 = ActorsMergeable.ofAuthor(dev2Name)
        val reviewerActor2 = ActorsMergeable.ofReviewer(dev2Name)
        val actual2 = actual.merge(authorActor2, false)
        assertEquals(ActorsMergeable(setOf(dev1Name, dev2Name), setOf(dev1Name)), actual2)
        val actual3 = actual2.merge(reviewerActor2, false)
        assertEquals(fullyLoaded,  actual3)

        val actual4 = actual3.merge(actual2, false).merge(actual, false)
        assertEquals(fullyLoaded, actual4)
    }
}