import stats.TimeGrouping
import utils.toBeginningOfGrouping
import java.time.OffsetDateTime
import java.time.ZoneOffset
import kotlin.test.Test
import kotlin.test.assertEquals

class OffsetDateTimeUtilsTests {

    @Test
    fun testBinning() {
        val yr = 2021
        val mo = 4
        val d = 7
        val hr = 17
        val min = 22
        val sec = 6
        val nano = 150
        val z = ZoneOffset.UTC
        val time = OffsetDateTime.of(yr, mo, d, hr, min, sec, nano, z)
        assertEquals(
            OffsetDateTime.of(yr, mo, d, hr, 0, 0, 0, z),
            time.toBeginningOfGrouping(TimeGrouping.Hours)
        )
        assertEquals(
            OffsetDateTime.of(yr, mo, d, 0, 0, 0, 0, z),
            time.toBeginningOfGrouping(TimeGrouping.Days)
        )
        assertEquals(
            OffsetDateTime.of(yr, mo, 4, 0, 0, 0, 0, z),
            time.toBeginningOfGrouping(TimeGrouping.Weeks)
        )
        assertEquals(
            OffsetDateTime.of(yr, mo, 1, 0, 0, 0, 0, z),
            time.toBeginningOfGrouping(TimeGrouping.Months)
        )
    }
}