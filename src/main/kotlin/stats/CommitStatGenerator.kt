package stats

import models.CommitData
import models.DataPoint
import models.mergeable.*
import normalizers.KeyNormalizer
import normalizers.PassthroughNormalizer
import kotlin.math.abs
import java.time.OffsetDateTime

class CommitStatGenerator(
    private val commits: Collection<CommitData>,
    private val authorNormalizer: KeyNormalizer = PassthroughNormalizer(),
    private val usernameNormalizer: KeyNormalizer = PassthroughNormalizer(),
) {
    companion object {
        const val TOTAL_KEYWORD = "Total"
    }


    fun statsOverTime(grouping: Grouping, type: StatType): Map<String, List<DataPoint<OffsetDateTime>>> {
        val results = mutableMapOf<String, MutableList<DataPoint<OffsetDateTime>>>()
        val runningStat = mutableMapOf<String, Mergeable>()

        val typeName = when (type) {
            StatType.Activities -> "Activities"
            StatType.Commits -> "Commits"
            StatType.Files -> "Files"
            StatType.Lines -> "Lines"
            StatType.LineVolume -> "LineVolume"
            StatType.TotalActivities -> "Total Activities"
            StatType.TotalCommits -> "Total Commits"
            StatType.TotalFiles -> "Total Files"
            StatType.TotalLines -> "Total Lines"
        }

        val rawStatEntries = mutableListOf<RawStatEntries>()

        commits.forEach { commitData ->
            val generalKey = when (grouping) {
                Grouping.ByAuthor -> authorNormalizer.transform(commitData.authorName, commitData.timestamp)
                Grouping.ByAuthorEmail -> authorNormalizer.transform(commitData.authorEmail, commitData.timestamp)
                Grouping.Total -> TOTAL_KEYWORD
            }

            when (type) {
                StatType.TotalCommits, StatType.Commits -> rawStatEntries.add(
                    RawStatEntries(generalKey, commitData.timestamp, 1.toLongMergeable(typeName))
                )
                StatType.TotalFiles -> rawStatEntries.add(
                    RawStatEntries(generalKey, commitData.timestamp, commitData.filesTotal.toLongMergeable(typeName))
                )
                StatType.Files -> rawStatEntries.add(
                    RawStatEntries(generalKey, commitData.timestamp, (commitData.filesAdded - commitData.filesRemoved).toLongMergeable(typeName))
                )
                StatType.TotalLines ->  rawStatEntries.add(
                    RawStatEntries(generalKey, commitData.timestamp, commitData.linesTotal.toLongMergeable(typeName))
                )
                StatType.LineVolume -> rawStatEntries.add(
                    RawStatEntries(generalKey, commitData.timestamp, (abs(commitData.linesAdded) + abs(commitData.linesRemoved)).toLongMergeable(typeName))
                )
                StatType.Lines -> rawStatEntries.add(
                    RawStatEntries(generalKey, commitData.timestamp, InsertionsDeletionsMergeable(commitData.linesAdded, commitData.linesRemoved))
                )
                StatType.Activities, StatType.TotalActivities -> {
                    rawStatEntries.add(RawStatEntries(generalKey, commitData.timestamp, ActivitiesMergeable(1, 0)))

                    commitData.coAuthors.forEach {
                        val localKey = if (grouping == Grouping.Total) {
                            generalKey
                        } else {
                            authorNormalizer.transform(it, commitData.timestamp)
                        }
                        rawStatEntries.add(RawStatEntries(localKey, commitData.timestamp, ActivitiesMergeable(1, 0)))
                    }
                    commitData.reviewers.forEach {
                        val localKey = if (grouping == Grouping.Total) {
                            generalKey
                        } else {
                            usernameNormalizer.transform(it, commitData.timestamp)
                        }
                        rawStatEntries.add(RawStatEntries(localKey, commitData.timestamp, ActivitiesMergeable(0, 1)))
                    }
                }
            }

        }

        rawStatEntries.forEach {
            val presentValue = when (type) {
                StatType.TotalCommits -> runningStat.getOrDefault(
                    it.key,
                    0.toLongMergeable(typeName)
                ).merge(it.value, false)
                StatType.TotalActivities -> runningStat.getOrDefault(
                    it.key,
                    ActivitiesMergeable(0, 0)
                ).merge(it.value, false)
                else -> it.value
            }

            val series = results.getOrPut(it.key) { mutableListOf() }
            runningStat[it.key] = presentValue
            series.add(DataPoint(it.timestamp, presentValue))
        }

        return results
    }

    private data class RawStatEntries(
        val key: String,
        val timestamp: OffsetDateTime,
        val value: Mergeable
    )
}

