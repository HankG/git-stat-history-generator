package stats

enum class Grouping {
    ByAuthor,
    ByAuthorEmail,
    Total,
}

enum class StatType {
    Activities,
    Commits,
    Files,
    Lines,
    LineVolume,
    TotalActivities,
    TotalCommits,
    TotalFiles,
    TotalLines
}

fun StatType.isTotalType() = when(this) {
    StatType.Activities -> false
    StatType.Commits -> false
    StatType.Files -> false
    StatType.Lines -> false
    StatType.LineVolume -> false
    StatType.TotalActivities -> true
    StatType.TotalCommits -> true
    StatType.TotalFiles -> true
    StatType.TotalLines -> true
}

enum class TimeBin {
    DayOfMonth,
    DayOfWeek,
    MonthOfYear,
    TimeOfDay,
}

enum class TimeGrouping {
    Hours,
    Days,
    Weeks,
    Months
}
