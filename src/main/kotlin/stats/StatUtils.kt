package stats

import models.DataPoint
import models.ProgramSharedData
import models.mergeable.Mergeable
import utils.shiftToZone
import utils.toBeginningOfGrouping
import java.time.OffsetDateTime
import java.time.ZoneId

fun List<DataPoint<OffsetDateTime>>.intoTimeBins(
    binType: TimeBin,
    zone: ZoneId? = ZoneId.systemDefault()
): List<DataPoint<Int>> {
    val bins = mutableMapOf<Int, Mergeable>()
    val default = this.first().value.default()

    val (min, max) = when (binType) {
        TimeBin.DayOfWeek -> Pair(1, 7)
        TimeBin.DayOfMonth -> Pair(1, 31)
        TimeBin.MonthOfYear -> Pair(1, 12)
        TimeBin.TimeOfDay -> Pair(0, 23)
    }

    (min..max).forEach { bins[it] = default.copy() }

    this.forEach {
        val time = it.index.shiftToZone(zone)
        val bin = when (binType) {
            TimeBin.DayOfWeek -> time.dayOfWeek.value
            TimeBin.DayOfMonth -> time.dayOfMonth
            TimeBin.MonthOfYear -> time.monthValue
            TimeBin.TimeOfDay -> time.hour
        }
        val newValue = bins.getOrDefault(bin, default.copy()).merge(it.value, false)
        bins[bin] = newValue
    }

    return bins.map { DataPoint(it.key, it.value) }.sortedBy { it.index }
}

fun List<DataPoint<OffsetDateTime>>.intoTimeGroupings(
    groupingType: TimeGrouping,
    dataPointsAreCumulative: Boolean,
    zone: ZoneId? = null,
    startTime: OffsetDateTime = ProgramSharedData.NO_DATE_FIELD,
    stopTime: OffsetDateTime = ProgramSharedData.NO_DATE_FIELD,
): List<DataPoint<OffsetDateTime>> {
    val bins = mutableMapOf<OffsetDateTime, Mergeable>()
    val default = this.first().value.default()

    val start = if (startTime == ProgramSharedData.NO_DATE_FIELD) {
        this.minOf { it.index }
    } else {
        startTime
    }.shiftToZone(zone).toBeginningOfGrouping(groupingType)

    val stop = if (stopTime == ProgramSharedData.NO_DATE_FIELD) {
        this.maxOf { it.index }
    } else {
        stopTime
    }.shiftToZone(zone).toBeginningOfGrouping(groupingType)

    bins[start] = default.copy()
    bins[stop] = default.copy()
    var t = start
    while (t <= stop) {
        bins.putIfAbsent(t.toBeginningOfGrouping(groupingType), default.copy())
        t = when (groupingType) {
            TimeGrouping.Hours -> t.plusHours(1)
            else -> t.plusDays(1)
        }
    }

    this.forEach {
        val dateGrouping = it.index.shiftToZone(zone).toBeginningOfGrouping(groupingType)
        val newValue = bins[dateGrouping]?.merge(it.value, dataPointsAreCumulative)
            ?: throw IllegalAccessError("Error generating table, precomputed row didn't exist: $dateGrouping")
        bins[dateGrouping] = newValue
    }

    val results = bins.map { DataPoint(it.key, it.value) }.sortedBy { it.index }.toMutableList()

    if (dataPointsAreCumulative) {
        for (i in 1 until results.size) {
            val curValue = results[i].value
            val value = if (curValue.compareTo(curValue.default()) == 0)
                curValue.merge(results[i - 1].value, true)
            else
                curValue
            results[i] = DataPoint(results[i].index, value)
        }
    }

    return results
}

fun Map<String, List<DataPoint<OffsetDateTime>>>.totalByKey(
    dataPointsAreCumulative: Boolean = false,
    earliest: OffsetDateTime = ProgramSharedData.NO_DATE_FIELD,
    latest: OffsetDateTime = ProgramSharedData.NO_DATE_FIELD,
): List<DataPoint<String>> {
    return this.map { (key, value) ->
        val default = value.first().value.default()
        value.filter { earliest == ProgramSharedData.NO_DATE_FIELD || it.index >= earliest }
            .filter { latest == ProgramSharedData.NO_DATE_FIELD || it.index <= latest }
            .map { DataPoint(key, it.value) }
            .fold(DataPoint(key, default)) { a, b -> DataPoint(key, a.value.merge(b.value, dataPointsAreCumulative)) }
    }
}

fun <T> List<DataPoint<T>>.mergeAll(dataPointsAreCumulative: Boolean = false): Mergeable =
    this.map { it.value }
        .reduce { acc, mergeable -> acc.merge(mergeable, dataPointsAreCumulative) }

fun TimeBin.toCsvHeader(): String = when (this) {
    TimeBin.DayOfMonth -> (1..31).joinToString(", ")
    TimeBin.DayOfWeek -> "Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday"
    TimeBin.MonthOfYear -> (1..12).joinToString(", ")
    TimeBin.TimeOfDay -> (0..23).joinToString(", ")
}