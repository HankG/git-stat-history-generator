package subcommands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.validate
import com.github.ajalt.clikt.parameters.types.enum
import com.github.ajalt.clikt.parameters.types.int
import models.ProgramSharedData
import handleCsvOutput
import models.DataPoint
import models.mergeable.*
import stats.StatType
import stats.mergeAll
import utils.commitsBetweenDates

class UserSummary : CliktCommand(name = "user-summaries", help = "Summary data for user's repository interactions") {
    val programSharedData by requireObject<ProgramSharedData>()

    val byOption by option(
        "--by",
        help = "Summarize by author name(or mapped author names)[default], or email (default by author)"
    ).enum<CliGroupingOption>()
        .default(CliGroupingOption.Author)

    val topUsersOnly by option("--top-users", help = "Only include top X users by commit count (default all users)").int()
        .validate { it > 0 }

    override fun run() {
        val commits = programSharedData.commits
            .commitsBetweenDates(programSharedData.earliestDateTime, programSharedData.latestDateTime)

        val generator = buildStatGenerator(commits, programSharedData)

        val commitData = generator.statsOverTime(byOption.toAnalysisGrouping(), StatType.Commits)
            .mapValues { it.value.mergeAll() }

        val topKeys = if (topUsersOnly != null) {
            commitData.map { (k, v) -> DataPoint(k, v) }
                .sortedByDescending { it.value }
                .take(topUsersOnly!!)
                .map { it.index }
                .toSet()
        } else {
            commitData.keys
        }

        val deltaLineData = generator.statsOverTime(byOption.toAnalysisGrouping(), StatType.Lines)
            .mapValues { it.value.mergeAll() }

        val volumeData = generator.statsOverTime(byOption.toAnalysisGrouping(), StatType.LineVolume)
            .mapValues { it.value.mergeAll() }

        val activities = generator.statsOverTime(byOption.toAnalysisGrouping(), StatType.Activities)
            .mapValues { it.value.mergeAll() }

        var otherUsersCommits: Mergeable = LongMergeable(0, "")
        var otherUsersLines: Mergeable = InsertionsDeletionsMergeable(0, 0)
        var otherUsersVolume: Mergeable = LongMergeable(0, "")
        var otherUsersActivities: Mergeable = ActivitiesMergeable(0, 0)
        var hasOtherUserData = false
        val data = commitData.mapValues {
            val deltaLine = deltaLineData[it.key]
            val volumeLine = volumeData[it.key]
            val activitiesLine = activities[it.key]
            if (!topKeys.contains(it.key)) {
                hasOtherUserData = true
                otherUsersCommits = otherUsersCommits.merge(it.value, false)
                otherUsersLines = otherUsersLines.merge(deltaLine?:otherUsersLines.default(), false)
                otherUsersVolume = otherUsersVolume.merge(volumeLine?:otherUsersVolume.default(), false)
                otherUsersActivities = otherUsersActivities.merge(activitiesLine?:otherUsersActivities.default(), false)
            }
            val combinedHeader = it.value.getCsvHeader() + "," + deltaLine?.getCsvHeader() + "," + volumeLine?.getCsvHeader() + "," + activitiesLine?.getCsvHeader()
            val combinedValue = it.value.toCsvString() + "," + deltaLine?.toCsvString() + "," + volumeLine?.toCsvString() + "," + activitiesLine?.toCsvString()
            DataPoint(it.key, StringMergeable(combinedValue, combinedHeader))
        }.values
            .sortedBy { it.index }
            .filter { topKeys.contains(it.index) }
            .toMutableList()

        if (hasOtherUserData) {
            val combinedValue = otherUsersCommits.toCsvString() + "," + otherUsersLines.toCsvString() + "," + otherUsersVolume.toCsvString() + "," + otherUsersActivities.toCsvString()
            data.add(DataPoint("OTHER USERS", StringMergeable(combinedValue, "")))
        }

        handleCsvOutput(programSharedData, data, "User Summaries", "User")
    }
}