package subcommands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.validate
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.validate
import com.github.ajalt.clikt.parameters.types.enum
import com.github.ajalt.clikt.parameters.types.int
import handleCsvOutput
import models.DataPoint
import models.ProgramSharedData
import models.mergeable.StringMergeable
import stats.*
import stats.CommitStatGenerator.Companion.TOTAL_KEYWORD
import utils.*
import java.time.OffsetDateTime
import java.time.ZoneId
import kotlin.system.exitProcess

private val OTHER_USER_NAME = "Other"

class Timeline : CliktCommand(name = "timeline", help = "Timeline statistics") {
    val programSharedData by requireObject<ProgramSharedData>()

    val statType by option("--stat-type", help = "Type of timeline to generate (defaults to total lines)")
        .enum<StatType>()
        .default(StatType.TotalLines)

    val timeType by option("--time-units", help = "Time type for timeline (defaults to weeks)")
        .enum<TimeGrouping>()
        .default(TimeGrouping.Weeks)

    val userData by option("--users", help = "Data for users (default doesn't generate)")
        .flag(default = false)

    val noTotalData by option("--no-total", help = "Data for entire repository (default the total is generated)")
        .flag(default = false)

    val topUsersOnly by option("--top-users", help = "Only include top X users (default all users)").int()
        .validate { it > 0 }

    val noOtherUserGrouping by option(
        "--no-others-grouping",
        help = "When using 'top users' groups remaining stats into one category (default does grouping)"
    ).flag(default = false)

    val timeZoneOffset by argument(
        "timezone",
        help = "Timezone to shift to if specified such as UTC or EST or can use 'local' system default",
    ).validate { it == "local" || ZoneId.SHORT_IDS.containsKey(it) }

    override fun run() {
        val zone: ZoneId? = if (timeZoneOffset.isNotEmpty()) {
            if (timeZoneOffset == "local") {
                ZoneId.systemDefault()
            } else {
                ZoneId.of(timeZoneOffset, ZoneId.SHORT_IDS)
            }
        } else {
            null
        }

        if (topUsersOnly != null && !userData){
            echo("Need to specify `--users` in order for `--top-users` to have an effect. Output will not be as expected")
            exitProcess(-1)
        }

        if (userData && statType.isTotalType()) {
            echo("This stat type can only be run in a mode for totals, disable user level calculations")
            exitProcess(-1)
        }

        val commits = programSharedData.commits

        val generator = buildStatGenerator(commits, programSharedData)
        val stats = mutableMapOf<String, List<DataPoint<OffsetDateTime>>>()

        if (userData) {
            generator.statsOverTime(Grouping.ByAuthor, statType)
                .forEach { (k, v) -> stats[k] = v }
        }

        var needOtherData = !noOtherUserGrouping && userData

        if (topUsersOnly != null) {
            val totals = stats
                .totalByKey(earliest = programSharedData.earliestDateTime, latest = programSharedData.latestDateTime)
                .sortedByDescending { it.value }
                .take(topUsersOnly!!)
                .map { it.index }
                .toSet()
            val otherKeys = stats.keys.filter { !totals.contains(it) }
            needOtherData = needOtherData && otherKeys.isNotEmpty()
            otherKeys.forEach { stats.remove(it) }
        }

        generator.statsOverTime(Grouping.Total, statType)
            .forEach { (k, v) -> stats[k] = v }

        if (stats.isEmpty()) {
            echo("No stats were generated, skipping timeline generation")
            return
        }

        val earliestDate = stats.map { s -> s.value.minOf { it.index } }.minOf { it }
        val latestDate = stats.map { s -> s.value.maxOf { it.index } }.maxOf { it }

        stats.forEach { (k, v) ->
            stats[k] = v.intoTimeGroupings(timeType, statType.isTotalType(), zone, earliestDate, latestDate)
                .dataPointsBetweenDates(programSharedData.earliestDateTime, programSharedData.latestDateTime)
        }

        val times = stats.values.first().map { it.index }
        val dataSample = stats.values.first().first().value
        val tabularData = mutableListOf<DataPoint<OffsetDateTime>>()
        val totalsStats = stats.remove(TOTAL_KEYWORD) ?: listOf()

        val headerPieces = stats.keys.map { dataSample.getCsvHeader(it) }.toMutableList()
        if (userData && needOtherData) {
            headerPieces.add(dataSample.getCsvHeader(OTHER_USER_NAME))
        }

        if (!noTotalData) {
            headerPieces.add(dataSample.getCsvHeader(TOTAL_KEYWORD))
        }

        val header = headerPieces.joinToString(", ")

        times.forEachIndexed { i, time ->
            val values = stats.map { it.value[i].value }.toMutableList()
            if (userData && needOtherData) {
                val sum = values.reduce { acc, mergeable -> acc.merge(mergeable, false) }
                val othersValue = totalsStats[i].value.substract(sum)
                values.add(othersValue)
            }

            if (!noTotalData) {
                values.add(totalsStats[i].value)
            }
            val line = values.map { it.toCsvString() }.joinToString(", ")
            tabularData.add(DataPoint(time, StringMergeable(line, header)))
        }

        val titleDescription = when (statType) {
            StatType.Activities -> "Net Activities"
            StatType.Commits -> "Net Commits"
            StatType.Files -> "Net Files"
            StatType.Lines -> "Insertions/Deletions"
            StatType.LineVolume -> "Line Volume"
            StatType.TotalActivities -> "Total Activities"
            StatType.TotalCommits -> "Total Commits"
            StatType.TotalFiles -> "Total Files"
            StatType.TotalLines -> "Total Lines"
        }

        handleCsvOutput(
            programSharedData,
            tabularData,
            "$titleDescription Over Time",
            "Date"
        )
    }
}
