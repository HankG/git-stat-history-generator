package subcommands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import handleCsvOutput
import models.DataPoint
import models.ProgramSharedData
import models.mergeable.StringMergeable
import normalizers.NameMappingNormalizer
import utils.commitsBetweenDates
import java.time.OffsetDateTime


class UniqueUserTable : CliktCommand(name = "unique-users", help = "List of all unique author name/email") {
    val programSharedData by requireObject<ProgramSharedData>()

    override fun run() {
        val mapper = NameMappingNormalizer(programSharedData.realNameMappings, true)

        val uniqueUsers = programSharedData
            .commits
            .commitsBetweenDates(programSharedData.earliestDateTime, programSharedData.latestDateTime)
            .map { DataPoint(it.authorName, StringMergeable(it.authorEmail + ", " + mapper.transform(it.authorName, OffsetDateTime.now()), "Email, Today Mapping")) }
            .toSortedSet(compareBy { it.index })
        handleCsvOutput(programSharedData, uniqueUsers, "Name", "Name")
    }
}