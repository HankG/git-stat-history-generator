package subcommands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.validate
import com.github.ajalt.clikt.parameters.types.enum
import handleCsvOutput
import models.DataPoint
import models.ProgramSharedData
import models.mergeable.StringMergeable
import stats.*
import utils.commitsBetweenDates
import java.time.OffsetDateTime
import java.time.ZoneId

class CommitFrequencyStats : CliktCommand(name = "commit-frequency", help = "Commit frequency data generation") {
    val programSharedData by requireObject<ProgramSharedData>()

    val frequencyType by option("--frequency-type", help = "Type of time binning desired")
        .enum<TimeBin>()
        .default(TimeBin.DayOfWeek)

    val userData by option("--users", help = "Data for users (default doesn't generate)")
        .flag(default = false)

    val noTotalData by option("--no-total", help = "Data for entire repository (default the total is generated)")
        .flag(default = false)

    val timeZoneOffset by option(
        "--timezone",
        help = "Timezone to shift to if specified such as UTC or EST or can use 'local' system default"
    ).validate { it == "local" || ZoneId.SHORT_IDS.containsKey(it) }

    override fun run() {
        val commits = programSharedData.commits
            .commitsBetweenDates(programSharedData.earliestDateTime, programSharedData.earliestDateTime)

        val generator = buildStatGenerator(commits, programSharedData)
        val stats = mutableMapOf<String, List<DataPoint<OffsetDateTime>>>()

        if (userData) {
            generator.statsOverTime(Grouping.ByAuthor, StatType.Commits)
                .toSortedMap()
                .forEach { (k, v) -> stats[k] = v }
        }
        if (!noTotalData) {
            generator.statsOverTime(Grouping.Total, StatType.Commits)
                .forEach { (k, v) -> stats[k] = v }
        }


        val zone: ZoneId? = if (timeZoneOffset?.isNotEmpty() == true) {
            if (timeZoneOffset == "local") {
                ZoneId.systemDefault()
            } else {
                ZoneId.of(timeZoneOffset, ZoneId.SHORT_IDS)
            }
        } else {
            null
        }

        val binnedValues = stats.mapValues { it.value.intoTimeBins(frequencyType, zone) }

        val tabularForm = binnedValues.map { (k, v) ->
            val value = v.joinToString(",") { it.value.toCsvString() }
            DataPoint(k, StringMergeable(value, frequencyType.toCsvHeader()))
        }

        handleCsvOutput(programSharedData, tabularForm, "Total Lines Over Time", "Name")
    }
}