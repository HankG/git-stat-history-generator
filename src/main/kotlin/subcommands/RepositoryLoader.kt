package subcommands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.split
import com.github.ajalt.clikt.parameters.types.file
import com.github.michaelbull.result.*
import getCommitDataFromFile
import models.ProgramSharedData
import processors.GitHistoryWalkProcessor
import processors.GitSimpleHistoryWalkProcessor
import putData
import utils.mergeAndTagFilterAndSort
import kotlin.io.path.ExperimentalPathApi
import kotlin.system.exitProcess

class RepositoryLoader: CliktCommand(
    name = "load-repo",
    help = "Reading/Saving of a Git Repository"
) {
    val programSharedData by requireObject<ProgramSharedData>()

    val allowedExtensions by option(
        "--included-extensions",
        help = "Comma separated list of lower case file extensions to consider in counts. Only relevant for the deep analysis mode.")
        .split(",")

    val updateFrom by option("--update-archive", help = "Whether to try to load the save location and update from most recent commit")
        .flag(default = false)

    val excludedExtensions by option(
        "--excluded-extensions",
        help = "Comma separated list of lower case file extensions to exclude in counts. Only relevant for the deep analysis mode.")
        .split(",")

    val branchName by option(
        "-b", "--branch-name",
        help = "Branch name to process if reading directly from a repository (defaults to master)"
    ).default("main")

    val gitDirectoryPath by argument("repository-folder", help = "Path to the root of the repository")
        .file(mustExist = true, mustBeReadable = true, canBeDir = true, canBeFile = false, canBeSymlink = false)

    val ignoreMerges by option("--ignore-merges", help = "Ignore merges when generating results")
        .flag(default = false)

    val ignoreTags by option("--ignore-tags", help = "Ignore tags when generating results (tags can skew totals by double counting commit summaries)")
        .flag(default = false)

    val includeHiddenFiles by option("--include-hidden", help = "Include files/folders with leading dot when doing deep walk only, default it does not.")
        .flag(default = false)

    val save by option("-s", "--save", help = "Path to save the processed git archive to")
        .file(mustExist = false, mustBeReadable = false, canBeDir = false, canBeFile = true, canBeSymlink = false)

    val bruteForce by option(
        "-f", "--brute-force",
        help = "Whether to process every commit for running total line/file processing or just look at commit log data." +
                "Repos with lots of history stomping squashes will have bad statistics if not run deeply." +
                "Deep analysis takes a lot longer to run since the repo has to walk through every commit change." +
                "Default is to run a shallow analysis just looking at the detailed log history.")
        .flag(default = false)


    @OptIn(ExperimentalPathApi::class)
    override fun run() {

        val cachedCommits = if (updateFrom) {
            if (save == null) {
                echo("Must specify an archive to update from and save to. Aborting")
                exitProcess(-1)
            }

            if (save?.exists() == false) {
                echo("Must specify an existing archive to update from and save to. Aborting")
                exitProcess(-1)
            }
            getCommitDataFromFile(save).mergeAndTagFilterAndSort(false, false)
                .sortedBy { it.timestamp }
        } else {
            listOf()
        }

        val includedExtensions = allowedExtensions?.map{ it.trim().toLowerCase() }?: listOf()
        val excludedExtensions = excludedExtensions?.map{ it.trim().toLowerCase() }?: listOf()

        val result = if (bruteForce) {
            GitHistoryWalkProcessor(gitDirectoryPath, includedExtensions, excludedExtensions, includeHiddenFiles, cachedCommits).process(branchName)
        } else {
            GitSimpleHistoryWalkProcessor(gitDirectoryPath, cachedCommits).process(branchName)
        }

        val readCommits = result.get()?:result.getError()?: listOf()

        val filteredCommits = readCommits.mergeAndTagFilterAndSort(ignoreMerges, ignoreTags)

        programSharedData.commits.addAll(filteredCommits)
        if (save != null) {
            readCommits.putData(save!!)
        }

        if (result.getError() != null) {
            echo("Pull resulted in error. Attempt to load with a refresh")
            exitProcess(-1)
        }
    }
}