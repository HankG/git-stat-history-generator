package subcommands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.findOrSetObject
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.validate
import com.github.ajalt.clikt.parameters.types.file
import getNameMappingsFromFile
import models.CommitData
import models.ProgramSharedData
import normalizers.NameMappingNormalizer
import normalizers.PassthroughNormalizer
import stats.CommitStatGenerator
import utils.toOffsetDateTime
import java.time.OffsetDateTime
import kotlin.system.exitProcess

class MainProgram :
    CliktCommand(
        name = "git-stat-history",
        allowMultipleSubcommands = true,
        printHelpOnEmptyArgs = true,
        invokeWithoutSubcommand = true
    ) {

    val realNameMappings by option(
        "--real-name-mappings",
        help = "Mapping file of real names to values (like companies or common names)"
    ).file(mustExist = true, mustBeReadable = false, canBeDir = false, canBeFile = true, canBeSymlink = false)

    val userNameMappings by option(
        "--user-name-mappings",
        help = "Mapping file of real names to values (like companies or common names)"
    ).file(mustExist = true, mustBeReadable = false, canBeDir = false, canBeFile = true, canBeSymlink = false)

    val earliestDateOption by option(
        "--earliest-date",
        help = "ISO formatted date/time '2020-10-16T00:00:44+09:00' earliest date for analysis"
    )
        .validate { OffsetDateTime.parse(it) }

    val latestDateOption by option(
        "--latest-date",
        help = "ISO formatted date/time '2020-10-16T00:00:44+09:00' earliest date for analysis"
    )
        .validate { OffsetDateTime.parse(it) }

    val outputCsv by option("--output-csv", help = "Toggle output to the specified CSV file")
        .file()

    val outputCli by option("--output-cli", help = "Whether to output text data to command line").flag()

    val programSharedData by findOrSetObject {
        ProgramSharedData(
            realNameMappings = getNameMappingsFromFile(realNameMappings, false),
            userNameMappings = getNameMappingsFromFile(realNameMappings, true),
            earliestDateTime = earliestDateOption?.toOffsetDateTime() ?: ProgramSharedData.NO_DATE_FIELD,
            latestDateTime = latestDateOption?.toOffsetDateTime() ?: ProgramSharedData.NO_DATE_FIELD,
            outputCli = outputCli,
            outputCsv = outputCsv != null,
            outputCsvFilename = outputCsv,
        )
    }

    override fun run() {
        val needsLoadingData = currentContext.invokedSubcommand !is UniqueExtensions
        val isLoadingData = currentContext.invokedSubcommand is ArchiveLoader
                || currentContext.invokedSubcommand is RepositoryLoader

        if (needsLoadingData && !isLoadingData) {
            echo("Need to run with either loading/processing a repository or an archive file")
            exitProcess(-1)
        }

        programSharedData.commits.clear()
    }
}

enum class CliGroupingOption {
    Author,
    Email,
}

fun buildStatGenerator(commits: List<CommitData>, programSharedData: ProgramSharedData): CommitStatGenerator {
    val realNameNormalizer = if (programSharedData.realNameMappings.isEmpty()) {
        PassthroughNormalizer()
    } else {
        NameMappingNormalizer(programSharedData.realNameMappings, true)
    }

    val userNameNormalizer = if (programSharedData.userNameMappings.isEmpty()) {
        PassthroughNormalizer()
    } else {
        NameMappingNormalizer(programSharedData.userNameMappings, true)
    }

    return CommitStatGenerator(commits, realNameNormalizer, userNameNormalizer)
}

fun CliGroupingOption.toAnalysisGrouping(): stats.Grouping = when (this) {
    CliGroupingOption.Author -> stats.Grouping.ByAuthor
    CliGroupingOption.Email -> stats.Grouping.ByAuthorEmail
}