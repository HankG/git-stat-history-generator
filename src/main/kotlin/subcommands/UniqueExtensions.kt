package subcommands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.file
import models.ProgramSharedData
import processors.GitUniqueExtensionProcessor
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.system.exitProcess

class UniqueExtensions: CliktCommand(
    name = "unique-file-extensions",
    help = "Determine the unique file extensions in a repository"
) {
    val programSharedData by requireObject<ProgramSharedData>()

    val branchName by option(
        "-b", "--branch-name",
        help = "Branch name to process if reading directly from a repository (defaults to master)"
    ).default("master")

    val gitDirectoryPath by argument("repository-folder", help = "Path to the root of the repository")
        .file(mustExist = true, mustBeReadable = true, canBeDir = true, canBeFile = false, canBeSymlink = false)

    val walk by option(
        "-w", "--walk",
        help = "Walk entire commit history for a branch or just use the head (default just the head)"
    )
        .flag(default = false)

    override fun run() {
        val extensions = GitUniqueExtensionProcessor(gitDirectoryPath)
            .process(branchName, walk)
            .toList()
            .sorted()

        if (programSharedData.outputCli) {
            echo("Unique extensions")
            extensions.forEach { echo(it) }
        }

        if (programSharedData.outputCsv) {
            Files.writeString(programSharedData.outputCsvFilename!!.toPath(), extensions.joinToString(System.lineSeparator()))
        }

        exitProcess(0)
    }
}