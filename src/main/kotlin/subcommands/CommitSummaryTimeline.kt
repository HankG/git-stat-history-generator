package subcommands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import handleCsvOutput
import models.CommitData
import models.DataPoint
import models.ProgramSharedData
import models.mergeable.StringMergeable
import utils.commitsBetweenDates

class CommitSummaryTimeline : CliktCommand(
    name = "commit-summary-timeline",
    help = "Timeline of raw commit summary data (no binning or grouping)"
) {
    val programSharedData by requireObject<ProgramSharedData>()

    override fun run() {
        val data = programSharedData.commits
            .commitsBetweenDates(programSharedData.earliestDateTime, programSharedData.latestDateTime)
            .map { DataPoint(it.timestamp, StringMergeable(it.toCsv(), it.toCsvHeader())) }
            .sortedBy { it.index }

        handleCsvOutput(
            programSharedData,
            data,
            "Commit Summary Timeline",
            "Date"
        )
    }
}

fun CommitData.toCsvHeader(separator: String = ", "): String =
    listOf(
        "Hash",
        "Files Changed",
        "Files Added",
        "Files Removed",
        "Files Renamed",
        "Files Total",
        "Lines Added",
        "Lines Removed",
        "Lines Total",
        "Author Email",
        "Author",
        "Is Merge",
        "Is Tag",
    ).joinToString(separator)

fun CommitData.toCsv(separator: String = ", "): String =
    listOf(
        this.gitHash,
        this.filesChanged,
        this.filesAdded,
        this.filesRemoved,
        this.filesRenamed,
        this.filesTotal,
        this.linesAdded,
        this.linesRemoved,
        this.linesTotal,
        this.authorEmail,
        this.authorName,
        this.isMerge,
        this.isTag
    ).joinToString(separator)
