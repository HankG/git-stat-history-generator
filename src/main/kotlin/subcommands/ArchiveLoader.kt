package subcommands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.file
import getCommitDataFromFile
import models.ProgramSharedData
import utils.mergeAndTagFilterAndSort

class ArchiveLoader: CliktCommand(
    name = "load-archive",
    help = "Load the archive of a previously processed repository"
) {
    val programSharedData by requireObject<ProgramSharedData>()

    val archiveFilePath by argument("archive-file", help = "Path to the archive from a previous run")
        .file(mustExist = true, mustBeReadable = false, canBeDir = false, canBeFile = true, canBeSymlink = false)

    val ignoreMerges by option("--ignore-merges", help = "Ignore merges when generating results")
        .flag(default = false)

    val ignoreTags by option("--ignore-tags", help = "Ignore tags when generating results (tags can skew totals by double counting commit summaries)")
        .flag(default = false)

    override fun run() {
        val readCommits = getCommitDataFromFile(archiveFilePath).mergeAndTagFilterAndSort(ignoreMerges, ignoreTags)
        programSharedData.commits.addAll(readCommits)
    }
}