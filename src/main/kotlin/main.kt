import com.github.ajalt.clikt.core.*
import subcommands.*

fun main(args: Array<String>) = MainProgram().subcommands(
    ArchiveLoader(),
    CommitFrequencyStats(),
    CommitSummaryTimeline(),
    RepositoryLoader(),
    Timeline(),
    UniqueUserTable(),
    UniqueExtensions(),
    UserSummary(),
).main(args)





