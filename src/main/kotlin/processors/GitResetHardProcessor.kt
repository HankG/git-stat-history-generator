package processors

import GIT_COMMAND
import java.io.File

class GitResetHardProcessor(val workingDirectory: File) {

    fun reset(branchNameToForce: String = ""): Boolean {
        val arguments = mutableListOf("reset", "--hard")
        if (branchNameToForce.isNotEmpty()) arguments.add(branchNameToForce)
        return ProcessRunner(workingDirectory).run(GIT_COMMAND, arguments).returnCode == ProcessRunner.SUCCESS_CODE
    }
}