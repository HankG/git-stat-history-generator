package processors

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import models.CommitData
import java.io.File
import com.github.michaelbull.result.Result

class GitSimpleHistoryWalkProcessor(
    private val workingDirectory: File,
    private val previousCache: List<CommitData>,
) {
    fun process(branchName: String): Result<List<CommitData>, List<CommitData>> {
        if(!GitCheckoutProcessor(workingDirectory).checkout(branchName)) {
            return Err(listOf())
        }

        val cachedMap = previousCache.associateBy { it.gitHash to it }
        val newCommits = GitLogProcessor(workingDirectory).process()
            .filter { !cachedMap.containsKey(it.gitHash) }
            .sortedBy { it.timestamp }
        val earliestNewCommit = newCommits.first()
        val nonOverlapCache = previousCache.filter { it.timestamp < earliestNewCommit.timestamp }
        val finalCommitData = mutableListOf<CommitData>()
        finalCommitData.addAll(nonOverlapCache)
        val cachedLastCommit = nonOverlapCache.lastOrNull()
        val finalCommitDataOffset = finalCommitData.size

        val initFinalPoint = if (cachedLastCommit == null) {
            newCommits[0].copy(
                linesTotal = newCommits[0].linesAdded - newCommits[0].linesRemoved,
                filesTotal = newCommits[0].filesAdded - newCommits[0].filesRemoved,
            )
        } else {
            updateLineCountStatsWithPrevious(cachedLastCommit, newCommits[0])
        }
        finalCommitData.add(initFinalPoint)

        for(i in 1 until newCommits.size) {
            val last = finalCommitData[i - 1 + finalCommitDataOffset]
            val current = newCommits[i]
            finalCommitData.add(updateLineCountStatsWithPrevious(last, current))
        }

        return Ok(finalCommitData)
    }

    private fun updateLineCountStatsWithPrevious(last: CommitData, current: CommitData): CommitData {
        val totalLines = if(current.isMerge || current.isTag)
            last.linesTotal
        else
            last.linesTotal + current.linesAdded - current.linesRemoved
        val totalFiles = if(current.isMerge || current.isTag)
            last.filesTotal
        else
            last.filesTotal + current.filesAdded - current.filesRemoved

        return current.copy(filesTotal = totalFiles, linesTotal = totalLines)
    }
}