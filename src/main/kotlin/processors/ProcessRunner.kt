package processors

import com.github.ajalt.clikt.output.TermUi.echo
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader

class ProcessRunner(private val workingDirectory: File) {
    companion object {
        const val SUCCESS_CODE = 0
    }
    data class Results(
        val outputs: List<String>,
        val returnCode: Int,
    )

    fun run(command: String, arguments: List<String>): Results {
        val commandElements = listOf(command) + arguments
        val processBuilder = ProcessBuilder()
        processBuilder.directory(workingDirectory)
        processBuilder.command(commandElements)

        val outputs = mutableListOf<String>()
        val process = processBuilder.start()
        with(BufferedReader(InputStreamReader(process.inputStream))) {
            var line = readLine()
            while (line != null) {
                outputs.add(line)
                line = readLine()
            }
        }

        val exitCode = process.waitFor()
        if (exitCode != SUCCESS_CODE) {
            echo("Error code for command: $commandElements")
        }
        return Results(outputs, exitCode)
    }
}