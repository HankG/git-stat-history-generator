package processors

import GIT_COMMAND
import processors.ProcessRunner.Companion.SUCCESS_CODE
import java.io.File

class GitCheckoutProcessor(val workingDirectory: File) {

    fun checkout(hashOrBranchName: String): Boolean {
        val arguments = listOf("checkout", hashOrBranchName)
        return ProcessRunner(workingDirectory).run(GIT_COMMAND, arguments).returnCode == SUCCESS_CODE
    }
}