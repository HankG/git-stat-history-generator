package processors

import com.github.ajalt.clikt.output.TermUi.echo
import java.io.File
import java.lang.IllegalArgumentException

class GitUniqueExtensionProcessor(private val workingDirectory: File) {
    fun process(branchName: String, walk: Boolean): Set<String> {
        val extensions = mutableSetOf<String>()
        if (!GitCheckoutProcessor(workingDirectory).checkout(branchName)) {
            throw IllegalArgumentException("Trouble checking out $branchName in git folder $workingDirectory")
        }

        GitLsFilesProcessor(workingDirectory).process().forEach { extensions.add(it.toFile().extension.toLowerCase()) }
        var lastStatusTime = 0L
        val statusIntervalMS = 60_000L
        if (walk) {
            val commits = GitLogProcessor(workingDirectory).process()
            commits.forEachIndexed { index, header ->
                val now = System.currentTimeMillis()
                val dt = now - lastStatusTime
                if (dt > statusIntervalMS) {
                    echo("Processing commit $index / ${commits.size}: ${header.gitHash}")
                    lastStatusTime = now
                }
                if (!GitCheckoutProcessor(workingDirectory).checkout(header.gitHash)) {
                    echo("Error checking out this revision, may cause result errors later: ${header.gitHash}")
                }
                val newExts = GitLsFilesProcessor(workingDirectory).process().map { it.toFile().extension.toLowerCase()}
                extensions.addAll(newExts)
                if (!GitResetHardProcessor(workingDirectory).reset(branchName)) {
                    echo("Error resetting repository, run is probably going to have errors moving forward")
                }
            }
        }

        return extensions
    }
}