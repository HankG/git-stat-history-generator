package processors

import GIT_COMMAND
import SEPARATOR
import com.github.ajalt.clikt.output.TermUi
import isHeaderLine
import isStatusLine
import models.CommitData
import models.CommitSummaryData
import parseToHeaderData
import parseToSummaryData
import processors.ProcessRunner.Companion.SUCCESS_CODE
import java.io.File

class GitLogProcessor(
    val workingDirectory: File,
    val sinceCommitHash: String = "",
    ) {
    private val CREATE_MODE_STRING = "create mode"
    private val DELETE_MODE_STRING = "delete mode"
    private val RENAME_MODE_STRING = "rename"
    private val SINGLE_FILE_STAT_SEPERATOR = "⁓"

    fun process(): List<CommitData> {
        val arguments = mutableListOf("log",
            "--pretty=format:new_commit:%H${SEPARATOR}%P${SEPARATOR}%D${SEPARATOR}%aN${SEPARATOR}%ae${SEPARATOR}%cI${SEPARATOR}%s${SEPARATOR}%b",
            "--reverse",
            "--encoding=UTF-8",
            "--stat",
            "--summary"
        )

        if (sinceCommitHash.isNotEmpty()) {
            arguments.add("$sinceCommitHash..HEAD")
        }

        val results = ProcessRunner(workingDirectory).run(GIT_COMMAND, arguments)
        if (results.returnCode != SUCCESS_CODE) {
            TermUi.echo("Error code running git log command: ${results.returnCode}")
            return listOf()
        }

        val commits = mutableListOf<CommitData>()
        var currentCommit = CommitData.NO_DATA
        results.outputs.forEach { line ->
            when {
                line.isHeaderLine() -> {
                    if (currentCommit != CommitData.NO_DATA) {
                        commits.add(currentCommit)
                    }

                    with(line.parseToHeaderData()) {
                        currentCommit = CommitData(
                            gitHash,
                            parentHash,
                            authorName,
                            authorEmail,
                            timestamp = timestamp,
                            isTag = isTag,
                            isMerge = isMerge,
                            coAuthors = coAuthors,
                            reviewers = reviewers,
                        )
                    }
                }

                line.isStatusLine() -> {
                    val summaryData = line.parseToSummaryData()
                    if (summaryData != CommitSummaryData.NO_DATA) {
                        currentCommit = currentCommit.copy(
                            filesChanged = summaryData.filesChanged,
                            linesAdded = summaryData.linesAdded,
                            linesRemoved = summaryData.linesRemoved,
                        )
                    }
                }

                line.contains(SINGLE_FILE_STAT_SEPERATOR) -> { /* do nothing but short circuit */
                }

                line.trim().startsWith(CREATE_MODE_STRING) -> {
                    currentCommit = currentCommit.copy(filesAdded = currentCommit.filesAdded + 1)
                }
                line.trim().startsWith(DELETE_MODE_STRING) -> {
                    currentCommit = currentCommit.copy(filesRemoved = currentCommit.filesRemoved + 1)
                }
                line.trim().startsWith(RENAME_MODE_STRING) -> {
                    currentCommit = currentCommit.copy(filesRenamed = currentCommit.filesRenamed + 1)
                }

            }
        }

        if (currentCommit != CommitData.NO_DATA) {
            commits.add(currentCommit)
        }

        return commits
    }
}