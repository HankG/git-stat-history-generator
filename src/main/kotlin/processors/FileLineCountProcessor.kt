package processors

import com.github.ajalt.clikt.output.TermUi
import com.github.ajalt.clikt.output.TermUi.echo
import java.io.File
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Paths

class FileLineCountProcessor(
    private val workingDirectory: File,
    private val tryAllEncodings: Boolean = true,
    private val verbose: Boolean = true,
) {
    companion object {
        val standardEncodingsToTry = listOf(
            "US-ASCII",
            "ISO-8859-1",
            "ISO-8859-2",
            "ISO-8859-3",
            "ISO-8859-4",
            "ISO-8859-16",
            "UTF-8",
            "UTF-16",
            "UTF-16BE",
            "UTF-16LE",
            "UTF-32",
            "UTF-32BE",
            "UTF-32LE",
            "ISO-2022-CN",
            "ISO-2022-JP",
            "ISO-2022-JP-2",
            "ISO-2022-KR",
            "ISO-8859-13",
            "ISO-8859-15",
            "ISO-8859-5",
            "ISO-8859-6",
            "ISO-8859-7",
            "ISO-8859-8",
            "ISO-8859-9",
        ).map { Charset.forName(it) }
    }
    fun process(file: File): Long {
        val fullPath = Paths.get(workingDirectory.absolutePath, file.path)
        try {
            return Files.readAllLines(fullPath).size.toLong()
        } catch (e: Exception) {
            if (!tryAllEncodings) {
                echo("$e Error trying to read file (may be binary but may be encoding problem), lines won't count: $file")
            }
        }

        if (!tryAllEncodings) {
            return 0
        }

        standardEncodingsToTry.forEach {
            try {
                val count = Files.readAllLines(fullPath, it).size.toLong()
                if (verbose) echo("Successfully processed using $it encoding (#lines = $count): $fullPath")
                return count
            } catch (e: Exception) {

            }
        }

        Charset.availableCharsets().values.forEach {
            try {
                val count = Files.readAllLines(fullPath, it).size.toLong()
                if (verbose) echo("Successfully processed using $it encoding (#lines = $count): $fullPath")
                return count
            } catch (e: Exception) {

            }
        }

        echo("No character sets worked for file $file, lines won't count")
        return 0
    }
}