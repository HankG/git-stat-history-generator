package processors

import GIT_COMMAND
import com.github.ajalt.clikt.output.TermUi.echo
import isStatusLine
import models.CommitData
import models.CommitSummaryData
import models.HeaderData
import models.toCommitData
import parseToSummaryData
import processors.ProcessRunner.Companion.SUCCESS_CODE
import java.io.File

class GitShowProcessor(val workingDirectory: File) {
    private val CREATE_MODE_STRING = "create mode"
    private val DELETE_MODE_STRING = "delete mode"
    private val RENAME_MODE_STRING = "rename"
    private val SINGLE_FILE_STAT_SEPERATOR = "|"

    fun process(header: HeaderData): CommitData {
        val arguments = listOf("show", "--stat", "--summary", header.gitHash)
        val results = ProcessRunner(workingDirectory).run(GIT_COMMAND, arguments)
        if (results.returnCode != SUCCESS_CODE) {
            echo("Error code running git show command, may skew results: ${results.returnCode}")
            return CommitData.NO_DATA
        }

        var foundStatusLine = false
        var currentCommit = header.toCommitData()
        results.outputs.forEach { line ->
            when {
                line.isStatusLine() -> {
                    foundStatusLine = true
                    val summaryData = line.parseToSummaryData()
                    if (summaryData != CommitSummaryData.NO_DATA) {
                        currentCommit = currentCommit.copy(
                            filesChanged = summaryData.filesChanged,
                            linesAdded = summaryData.linesAdded,
                            linesRemoved = summaryData.linesRemoved,
                        )
                    }
                }

                line.contains(SINGLE_FILE_STAT_SEPERATOR) -> { /* do nothing but short circuit */
                }

                line.trim().startsWith(CREATE_MODE_STRING) -> {
                    currentCommit = currentCommit.copy(filesAdded = currentCommit.filesAdded + 1)
                }
                line.trim().startsWith(DELETE_MODE_STRING) -> {
                    currentCommit = currentCommit.copy(filesRemoved = currentCommit.filesRemoved + 1)
                }
                line.trim().startsWith(RENAME_MODE_STRING) -> {
                    currentCommit = currentCommit.copy(filesRenamed = currentCommit.filesRenamed + 1)
                }

            }
        }
        if (!foundStatusLine) {
            echo("Didn't find a status line for this commit, may be processing error: ${header.gitHash} ")
        }

        return currentCommit
    }

}