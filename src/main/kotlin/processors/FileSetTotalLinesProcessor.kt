package processors

import java.io.File
import java.nio.file.Path

class FileSetTotalLinesProcessor(
    private val workingDirectory: File,
    private val allowedExtensions: List<String> = listOf(),
    private val excludedExtensions: List<String> = listOf(),
) {

    fun process(files: List<Path>): Long {
        val lcp = FileLineCountProcessor(workingDirectory, true)
        return files
            .map { it.toFile() }
            .filter { allowedExtensions.isEmpty() || allowedExtensions.contains(it.extension.toLowerCase()) }
            .filter { excludedExtensions.isEmpty() || !excludedExtensions.contains(it.extension.toLowerCase())}
            .map {
                lcp.process(it)
            }.sumOf { it }
    }
}