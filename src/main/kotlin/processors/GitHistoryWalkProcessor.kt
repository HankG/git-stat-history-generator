package processors

import com.github.ajalt.clikt.output.TermUi.echo
import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import models.CommitData
import java.io.File
import java.nio.file.Path
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.isHidden

@ExperimentalPathApi
class GitHistoryWalkProcessor(
    private val workingDirectory: File,
    private val allowedExtensionsForLineCounts: List<String>,
    private val excludedExtensionsForLineCounts: List<String>,
    private val includeHiddenFiles: Boolean,
    private val previousCache: List<CommitData>,
) {

    fun process(branchName: String): Result<List<CommitData>, List<CommitData>> {
        if(!GitCheckoutProcessor(workingDirectory).checkout(branchName)) {
            return Err(listOf())
        }

        val cachedMap = previousCache.associateBy { it.gitHash to it }
        val newCommits = GitLogProcessor(workingDirectory).process()
            .filter { !cachedMap.containsKey(it.gitHash) }
            .sortedBy { it.timestamp }
        val earliestNewCommit = newCommits.first()
        val nonOverlapCache = previousCache.filter { it.timestamp < earliestNewCommit.timestamp }

        var lastStatusTime = 0L
        val statusIntervalMS = 60_000L
        var erroredOut = false
        val rawCommitData = mutableListOf<CommitData>()
        for(index in newCommits.indices) {
            val commit = newCommits[index]
            val now = System.currentTimeMillis()
            val dt = now - lastStatusTime
            if (dt > statusIntervalMS) {
                echo("Processing commit $index / ${newCommits.size}: ${commit.gitHash}")
                lastStatusTime = now
            }
            if (!GitCheckoutProcessor(workingDirectory).checkout(commit.gitHash)) {
                echo("Error checking out this revision, may cause result errors later: ${commit.gitHash}")
                erroredOut = true
                break
            }

            val files = GitLsFilesProcessor(workingDirectory).process()
            val filteredFiles = files.filter { includeHiddenFiles || !it.pathIsHidden() }
            val totalLines = FileSetTotalLinesProcessor(workingDirectory, allowedExtensionsForLineCounts, excludedExtensionsForLineCounts)
                .process(filteredFiles)
            if (!GitResetHardProcessor(workingDirectory).reset(branchName)) {
                echo ("Error resetting repository, run is probably going to have errors moving forward")
                erroredOut = true
                break
            }

            rawCommitData.add(commit.copy(linesTotal = totalLines, filesTotal = files.size.toLong()))
        }

        if(!GitCheckoutProcessor(workingDirectory).checkout(branchName)) {
            echo("Attempted to reset back to head of requested branch but error was thrown")
        }

        if (rawCommitData.isEmpty()) {
            return if(erroredOut) Err(rawCommitData) else Ok(rawCommitData)
        }

        val finalCommitData = mutableListOf<CommitData>()
        finalCommitData.addAll(nonOverlapCache)
        if (nonOverlapCache.isEmpty()) {
            finalCommitData.add(rawCommitData[0])
        } else {
            finalCommitData.add(updateLineCountStatsWithPrevious(nonOverlapCache.last(), rawCommitData[0]))
        }

        for(i in 1 until rawCommitData.size) {
            val last = rawCommitData[i - 1]
            val current = rawCommitData[i]

            finalCommitData.add(updateLineCountStatsWithPrevious(last, current))
        }
        return if(erroredOut) Err(finalCommitData) else Ok(finalCommitData)
    }

    private fun updateLineCountStatsWithPrevious(last: CommitData, current: CommitData): CommitData {
        //that means it had a status line so don't need to put in some filler numbers for when stomped commits omit
        if (current.filesChanged != 0L || current.linesAdded != 0L || current.linesRemoved != 0L) {
            return current
        }

        val lineCountChange = current.linesTotal - last.linesTotal
        val fileCountChange = current.filesTotal - last.filesTotal

        val (linesAdded, linesRemoved) = if (lineCountChange > 0) Pair(lineCountChange, 0L) else Pair(0L, lineCountChange)
        val (filesAdded, filesRemoved) = if (fileCountChange > 0) Pair(fileCountChange, 0L) else Pair(0L, fileCountChange)
        return current.copy(
            linesAdded = linesAdded,
            linesRemoved = linesRemoved,
            filesAdded = filesAdded,
            filesRemoved = filesRemoved
        )
    }
}

@ExperimentalPathApi
fun Path.pathIsHidden(): Boolean {
    if (this.toFile().name != "." && this.isHidden()) {
        return true
    }

    if (this.parent == null) {
        return false
    }

    return this.parent.pathIsHidden()
}