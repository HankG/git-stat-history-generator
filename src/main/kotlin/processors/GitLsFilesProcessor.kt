package processors

import GIT_COMMAND
import processors.ProcessRunner.Companion.SUCCESS_CODE
import java.io.File
import java.nio.file.Path
import java.nio.file.Paths

class GitLsFilesProcessor(private val workingDirectory: File) {

    fun process(): List<Path> {
        val arguments = listOf("ls-files")
        val results = ProcessRunner(workingDirectory).run(GIT_COMMAND, arguments)
        if(results.returnCode != SUCCESS_CODE) {
            return listOf()
        }

        return results.outputs.map { Paths.get(it) }
    }
}