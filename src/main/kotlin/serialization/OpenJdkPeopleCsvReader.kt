package serialization

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import models.DateThresholdValue
import models.NameMapping
import java.io.File
import java.time.OffsetDateTime

object OpenJdkPeopleCsvReader {
    enum class NameKey {
        RealName,
        Username,
    }

    fun read(file: File, hasHeader: Boolean, nameKey: NameKey): List<NameMapping> {
        val nameIndex = 0
        val orgIndex = 1
        val userIndex = 2
        val blogIndex = 3

        val rows = csvReader().readAll(file)
        val actualRows = if (hasHeader)
            rows.subList(1, rows.size)
        else
            rows

        return actualRows.map {
            val name = when(nameKey) {
                NameKey.RealName -> it[nameIndex]
                NameKey.Username -> it[userIndex]
            }
            val orgName = it[orgIndex].trim()
            NameMapping(
                originalFieldValue = name,
                newFieldValues = listOf(
                    DateThresholdValue(
                        latestValidDate = OffsetDateTime.MIN,
                        newValue = orgName.ifEmpty { name.trim() }
                    )
                )
            )
        }
    }
}