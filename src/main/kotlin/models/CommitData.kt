package models

import kotlinx.serialization.Serializable
import serialization.OffsetDateTimeSerializer
import java.time.OffsetDateTime

@Serializable
data class CommitData(
    val gitHash: String = "",
    val parentHash: String = "",
    val authorName: String = "",
    val authorEmail: String = "",
    val coAuthors: List<String> = listOf(),
    val reviewers: List<String> = listOf(),
    @Serializable(with = OffsetDateTimeSerializer::class)
    val timestamp: OffsetDateTime = OffsetDateTime.now(),
    val linesAdded: Long = 0,
    val linesRemoved: Long = 0,
    val linesTotal: Long = 0,
    val filesAdded: Long = 0,
    val filesChanged: Long = 0,
    val filesRemoved: Long = 0,
    val filesRenamed: Long = 0,
    val filesTotal: Long = 0,
    val isTag: Boolean = false,
    val isMerge: Boolean = false,
) {
    companion object {
        val NO_DATA = CommitData()
    }
}
