package models

import java.io.File
import java.time.OffsetDateTime

data class ProgramSharedData(
    val commits: MutableList<CommitData> = mutableListOf(),
    val realNameMappings: List<NameMapping>,
    val userNameMappings: List<NameMapping>,
    val earliestDateTime: OffsetDateTime,
    val latestDateTime: OffsetDateTime,
    val outputCli: Boolean,
    val outputCsv: Boolean,
    val outputCsvFilename: File?,
) {
    companion object {
        val NO_DATE_FIELD = OffsetDateTime.MAX
    }
}