package models.mergeable

import java.lang.IllegalArgumentException

data class StringMergeable(val value: String, val header: String) : Mergeable {
    override fun copy(): Mergeable = StringMergeable(value, header)

    override fun default(): Mergeable = StringMergeable("", "Value")

    override fun merge(other: Mergeable, asMax: Boolean): Mergeable {
        if (other is StringMergeable) {
            return StringMergeable(this.value + other.value, header)
        }

        throw IllegalArgumentException("Cannot merge type: ${other::class}")
    }

    override fun substract(other: Mergeable): Mergeable {
        throw NotImplementedError("String mergeables doesn't support subtracting")
    }

    override fun toCsvString(): String = value

    override fun getCsvHeader(leader: String): String = "$leader $header"

    override fun compareTo(other: Mergeable): Int {
        if (other is StringMergeable) {
            return this.value.compareTo(other.value)
        }
        throw IllegalArgumentException("Cannot compare to type: ${other::class}")
    }

}