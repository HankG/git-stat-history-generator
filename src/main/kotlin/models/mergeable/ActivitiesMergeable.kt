package models.mergeable

import java.lang.IllegalArgumentException

data class ActivitiesMergeable(val authorships: Long, val reviews: Long) : Mergeable {
    override fun copy(): Mergeable = ActivitiesMergeable(this.authorships, this.reviews)

    override fun default(): Mergeable = ActivitiesMergeable(0, 0)

    override fun merge(other: Mergeable, asMax: Boolean): Mergeable {
        if (other is ActivitiesMergeable) {
            return if (asMax) {
                ActivitiesMergeable(
                    maxOf(this.authorships, other.authorships),
                    maxOf(this.reviews, other.reviews)
                )
            } else {
                ActivitiesMergeable(
                    this.authorships + other.authorships,
                    this.reviews + other.reviews
                )
            }
        }
        throw IllegalArgumentException("Cannot merge type: ${other::class}")
    }

    override fun substract(other: Mergeable): Mergeable {
        if (other is ActivitiesMergeable) {
            return ActivitiesMergeable(this.authorships - other.authorships, this.reviews - other.reviews)
        }

        throw IllegalArgumentException("Cannot subtract type: ${other::class}")
    }

    override fun toString(): String = "ActivitiesMergeable(authorships=$authorships, reviews=$reviews)"

    override fun toCsvString(): String = "$authorships, $reviews"

    override fun getCsvHeader(leader: String): String = "$leader Authorships, $leader Reviews"

    override fun compareTo(other: Mergeable): Int {
        if (other is ActivitiesMergeable) {
            val myMax = authorships + reviews
            val theirMax = other.authorships + other.reviews
            return myMax.compareTo(theirMax)
        }
        throw IllegalArgumentException("Cannot compare to type: ${other::class}")
    }

}