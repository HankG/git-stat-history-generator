package models.mergeable

interface Mergeable : Comparable<Mergeable> {

    fun copy(): Mergeable

    fun default(): Mergeable

    fun merge(other: Mergeable, asMax: Boolean): Mergeable

    fun substract(other: Mergeable): Mergeable

    fun toCsvString(): String

    fun getCsvHeader(leader: String = ""): String

}



