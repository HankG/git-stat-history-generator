package models.mergeable

import java.lang.IllegalArgumentException

data class LongMergeable(val value: Long, val header: String) : Mergeable {
    override fun copy(): Mergeable = LongMergeable(this.value, this.header)

    override fun default(): Mergeable = LongMergeable(0L, "")

    override fun merge(other: Mergeable, asMax: Boolean): Mergeable {
        if (other is LongMergeable) {
            val value = if (asMax) maxOf(this.value, other.value) else this.value + other.value
            return LongMergeable(value, this.header)
        }
        throw IllegalArgumentException("Cannot merge type: ${other::class}")
    }

    override fun substract(other: Mergeable): Mergeable {
        if (other is LongMergeable) {
            return LongMergeable(this.value - other.value, this.header)
        }

        throw IllegalArgumentException("Don't know how to subtract ${other::class}")
    }

    override fun toString(): String = "LongMergeable(header=$header, value=$value)"

    override fun toCsvString(): String = "$value"

    override fun getCsvHeader(leader: String): String = "$leader $header"

    override fun compareTo(other: Mergeable): Int {
        if (other is LongMergeable) {
            return this.value.compareTo(other.value)
        }
        throw IllegalArgumentException("Cannot compare to type: ${other::class}")
    }

}

fun Long.toLongMergeable(name: String): LongMergeable = LongMergeable(this, name)

fun Int.toLongMergeable(name: String): LongMergeable = LongMergeable(this.toLong(), name)