package models.mergeable

import java.lang.IllegalArgumentException

data class ActorsMergeable(val authors: Set<String>, val reviewers: Set<String>) : Mergeable {

    companion object {
        fun ofAuthor(name: String): ActorsMergeable = ActorsMergeable(setOf(name), setOf())

        fun ofReviewer(name: String): ActorsMergeable = ActorsMergeable(setOf(), setOf(name))
    }

    override fun copy(): Mergeable = ActorsMergeable(this.authors, this.reviewers)

    override fun default(): Mergeable = ActorsMergeable(setOf(), setOf())

    override fun merge(other: Mergeable, asMax: Boolean): Mergeable {
        if (other is ActorsMergeable) {
            val authors = this.authors + other.authors
            val reviewers = this.reviewers + other.reviewers
            return ActorsMergeable(authors, reviewers)
        }
        throw IllegalArgumentException("Cannot merge type: ${other::class}")
    }

    override fun substract(other: Mergeable): Mergeable {
        if (other is ActorsMergeable) {
            val authors = this.authors - other.authors
            val reviewers = this.reviewers - other.reviewers
            return ActorsMergeable(authors, reviewers)
        }

        throw IllegalArgumentException("Cannot subtract type: ${other::class}")
    }

    override fun toString(): String = "ActorsMergeable(authorsCount=${authors.size}, reviewers=${reviewers.size})"

    override fun toCsvString(): String = "${authors.size}, ${reviewers.size}"

    override fun getCsvHeader(leader: String): String = "$leader Author Count, $leader Reviewer Count"

    override fun compareTo(other: Mergeable): Int {
        if (other is ActorsMergeable) {
            val myMax = authors.size + reviewers.size
            val theirMax = other.authors.size + other.reviewers.size
            return myMax.compareTo(theirMax)
        }
        throw IllegalArgumentException("Cannot compare to type: ${other::class}")
    }

}