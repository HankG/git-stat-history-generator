package models.mergeable

import java.lang.IllegalArgumentException
import kotlin.math.abs

data class InsertionsDeletionsMergeable(val insertions: Long, val deletions: Long) : Mergeable {
    override fun copy(): Mergeable = InsertionsDeletionsMergeable(this.insertions, this.deletions)

    override fun default(): Mergeable = InsertionsDeletionsMergeable(0, 0)

    override fun merge(other: Mergeable, asMax: Boolean): Mergeable {
        if (other is InsertionsDeletionsMergeable) {
            return if (asMax) {
                InsertionsDeletionsMergeable(
                    maxOf(this.insertions, other.insertions),
                    maxOf(this.deletions, other.deletions)
                )
            } else {
                InsertionsDeletionsMergeable(
                    this.insertions + other.insertions,
                    this.deletions + other.deletions
                )
            }
        }
        throw IllegalArgumentException("Cannot merge type: ${other::class}")
    }

    override fun substract(other: Mergeable): Mergeable {
        if (other is InsertionsDeletionsMergeable) {
            return InsertionsDeletionsMergeable(this.insertions - other.insertions, this.deletions - other.deletions)
        }

        throw IllegalArgumentException("Cannot subtract type: ${other::class}")
    }

    override fun toString(): String = "InsertionsDeletionsMergeable(insertions=$insertions, deletions=$deletions)"

    override fun toCsvString(): String = "$insertions, -$deletions"

    override fun getCsvHeader(leader: String): String = "$leader Insertions, $leader Deletions"

    override fun compareTo(other: Mergeable): Int {
        if (other is InsertionsDeletionsMergeable) {
            val myMax = maxOf(abs(insertions), abs(deletions))
            val theirMax = maxOf(abs(other.insertions), abs(other.deletions))
            return myMax.compareTo(theirMax)
        }
        throw IllegalArgumentException("Cannot compare to type: ${other::class}")
    }

}