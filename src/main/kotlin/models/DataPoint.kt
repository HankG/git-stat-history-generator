package models

import models.mergeable.Mergeable

data class DataPoint<T>(
    val index: T,
    val value: Mergeable
) {
    fun toCsvHeader(indexName: String = "Index", leader: String = "") = "$indexName, ${value.getCsvHeader(leader)}"

    fun toCsvValue(): String = "$index, ${value.toCsvString()}"
}
