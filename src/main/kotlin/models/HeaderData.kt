package models

import java.time.OffsetDateTime

data class HeaderData(
    val gitHash: String = "",
    val parentHash: String = "",
    val authorName: String = "",
    val authorEmail: String = "",
    val coAuthors: List<String> = listOf(),
    val reviewers: List<String> = listOf(),
    val timestamp: OffsetDateTime = OffsetDateTime.now(),
    val isTag: Boolean = false,
    val isMerge: Boolean = false,
    val text: String = ""
) {
    companion object {
        val NO_DATA = HeaderData()
    }
}

fun HeaderData.toCommitData(): CommitData =
    CommitData(
        gitHash = gitHash,
        parentHash = parentHash,
        authorName = authorName,
        authorEmail = authorEmail,
        timestamp = timestamp,
        isTag = isTag,
        isMerge = isMerge,
    )
