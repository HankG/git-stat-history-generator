package models

data class CommitSummaryData(
    val filesChanged: Long = 0,
    val linesAdded: Long = 0,
    val linesRemoved: Long = 0,
) {
    companion object {
        val NO_DATA = CommitSummaryData()
    }
}
