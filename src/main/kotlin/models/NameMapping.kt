package models

import kotlinx.serialization.Serializable
import serialization.OffsetDateTimeSerializer
import java.time.OffsetDateTime

@Serializable
data class NameMapping(
    val originalFieldValue: String,
    val newFieldValues: List<DateThresholdValue>
)

@Serializable
data class DateThresholdValue(
    @Serializable(with = OffsetDateTimeSerializer::class)
    val latestValidDate: OffsetDateTime,
    val newValue: String
)