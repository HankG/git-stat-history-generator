package normalizers

import java.time.OffsetDateTime

class PassthroughNormalizer : KeyNormalizer {
    override fun transform(key: String, timestamp: OffsetDateTime): String = key
}