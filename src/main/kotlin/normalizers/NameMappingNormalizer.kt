package normalizers

import models.NameMapping
import java.lang.IllegalArgumentException
import java.time.OffsetDateTime

class NameMappingNormalizer(
    nameMapping: Collection<NameMapping>,
    val useDefaultIfNotFound: Boolean,
    val defaultValueIfNotFound: String = "UNKNOWN",
) : KeyNormalizer {
    private val mappings = nameMapping.associate { it.originalFieldValue to it.newFieldValues }

    override fun transform(key: String, timestamp: OffsetDateTime): String {
        val transformValue = mappings.getOrDefault(key, listOf())
            .sortedBy { it.latestValidDate }
            .lastOrNull { timestamp >= it.latestValidDate }

        if (transformValue != null) {
            return transformValue.newValue
        }

        if (useDefaultIfNotFound) {
            return defaultValueIfNotFound
        }

        throw IllegalArgumentException("Can't find name mapping for original value $key at $timestamp")
    }
}