package normalizers

import java.time.OffsetDateTime

interface KeyNormalizer {

    fun transform(key: String, timestamp: OffsetDateTime): String

}