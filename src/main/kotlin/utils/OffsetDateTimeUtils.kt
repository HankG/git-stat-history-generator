package utils

import stats.TimeGrouping
import java.time.OffsetDateTime
import java.time.ZoneId
import java.time.temporal.ChronoUnit


fun String.toOffsetDateTime(): OffsetDateTime = OffsetDateTime.parse(this)

fun OffsetDateTime.shiftToZone(zone: ZoneId?): OffsetDateTime =
    if (zone == null) this else this.atZoneSameInstant(zone).toOffsetDateTime()

fun OffsetDateTime.toBeginningOfGrouping(units: TimeGrouping): OffsetDateTime {
    if (units == TimeGrouping.Hours) {
        return this.truncatedTo(ChronoUnit.HOURS)
    }

    val atDay = this.truncatedTo(ChronoUnit.DAYS)
    if (units == TimeGrouping.Days) {
        return atDay
    }

    return if (units == TimeGrouping.Weeks) {
        atDay.minusDays(atDay.dayOfWeek.value.toLong())
    } else {
        atDay.minusDays((atDay.dayOfMonth - 1).toLong())
    }
}