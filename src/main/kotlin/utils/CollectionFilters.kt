package utils

import models.CommitData
import models.DataPoint
import models.ProgramSharedData
import java.time.OffsetDateTime

fun List<CommitData>.commitsBetweenDates(earliest: OffsetDateTime, latest: OffsetDateTime) =
    this.filter { earliest == ProgramSharedData.NO_DATE_FIELD || it.timestamp >= earliest }
        .filter { latest == ProgramSharedData.NO_DATE_FIELD || it.timestamp <= latest }

fun List<CommitData>.mergeAndTagFilterAndSort(ignoreMerges: Boolean, ignoreTags: Boolean): List<CommitData> =
    this.filter { !ignoreMerges || !it.isMerge }
        .filter { !ignoreTags || !it.isTag}
        .sortedBy { it.timestamp }

fun List<DataPoint<OffsetDateTime>>.dataPointsBetweenDates(earliest: OffsetDateTime, latest: OffsetDateTime) =
    this.filter { earliest == ProgramSharedData.NO_DATE_FIELD || it.index >= earliest }
        .filter { latest == ProgramSharedData.NO_DATE_FIELD || it.index <= latest }