import models.CommitSummaryData
import models.HeaderData
import java.lang.IllegalArgumentException
import java.time.OffsetDateTime

const val GIT_COMMAND = "git"
const val HEADER_START = "new_commit:"
const val SEPARATOR = '⸱'

fun String.parseToHeaderData(): HeaderData {
    if (!this.isHeaderLine()) {
        return HeaderData.NO_DATA
    }

    val gitHashIndex = 0
    val parentHashesIndex = 1
    val refNamesIndex = 2
    val authorIndex = 3
    val authorEmailIndex = 4
    val timestampIndex = 5
    val textStartIndex = 6
    val valueString = this.trim().substring(HEADER_START.length)
    val values = valueString.split(SEPARATOR)
    if (values.size < timestampIndex + 1) {
        throw IllegalArgumentException("Header data is not complete, need at least five elements: $valueString")
    }
    val hash = values[gitHashIndex]
    val parentHash = values[parentHashesIndex]
    val isMerge = parentHash.split("\\s+".toRegex()).size > 1
    val isTag = values[refNamesIndex].trim().startsWith("tag: ")
    val author = values[authorIndex]
    val authorEmail = values[authorEmailIndex]
    val timestamp = OffsetDateTime.parse(values[timestampIndex])
    val text = if (values.size > textStartIndex)
        values.subList(textStartIndex, values.size).joinToString(System.lineSeparator())
    else
        ""

    val coauthorText = "Co-authored-by:"
    val reviewerText = "Reviewed-by:"
    val allCoauthors = mutableListOf<String>()
    val allReviewers = mutableListOf<String>()
    text.split(System.lineSeparator()).forEach {
        val line = it.trim()
        if (line.startsWith(coauthorText)) {
            val authorFields = line.substring(coauthorText.length).split("<")
            allCoauthors.add(authorFields.first().trim())
        }

        if (line.startsWith("Reviewed-by:")) {
            val reviewers = line.substring(reviewerText.length).split(",")
            reviewers.forEach { r -> allReviewers.add(r.trim()) }
        }
    }

    return HeaderData(
        gitHash = hash,
        parentHash = parentHash,
        authorName = author,
        authorEmail = authorEmail,
        timestamp = timestamp,
        isTag = isTag,
        isMerge = isMerge,
        text = text,
        reviewers = allReviewers,
        coAuthors = allCoauthors
    )
}

fun String.parseToSummaryData(): CommitSummaryData {
    var filesChanged = 0L
    var linesAdded = 0L
    var linesRemoved = 0L
    if (!this.isStatusLine()) {
        return CommitSummaryData.NO_DATA
    }

    this.trim().split(',').forEach { element ->
        when {
            element.contains("changed") -> filesChanged = element.extractLeadingLong()
            element.contains("insertion") -> linesAdded = element.extractLeadingLong()
            element.contains("deletion") -> linesRemoved = element.extractLeadingLong()
        }
    }

    return CommitSummaryData(
        filesChanged = filesChanged,
        linesAdded = linesAdded,
        linesRemoved = linesRemoved,
    )
}

fun String.extractLeadingLong(): Long = this.trim().split("\\s+".toRegex()).first().toLong()

fun String.isHeaderLine(): Boolean = this.startsWith(HEADER_START)

fun String.isStatusLine(): Boolean =
    (this.contains(" files changed,") || this.contains(" file changed,"))
            && (this.contains(" insertions(+)")
            || this.contains(" insertion(+)")
            || this.contains(" deletions(-)")
            || this.contains(" deletion(-)"))

