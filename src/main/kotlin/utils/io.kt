import com.github.ajalt.clikt.output.TermUi
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import models.CommitData
import models.DataPoint
import models.NameMapping
import models.ProgramSharedData
import serialization.OpenJdkPeopleCsvReader
import java.io.File
import java.lang.IllegalArgumentException
import java.nio.file.Files

fun List<CommitData>.putData(file: File) {
    val json = Json { prettyPrint = true }.encodeToString(this)
    Files.writeString(file.toPath(), json)
}

fun getCommitDataFromFile(file: File?): List<CommitData> {
    if (file == null) return listOf()
    if (!file.exists()) return listOf()
    val jsonData = Files.readString(file.toPath()).trim()
    if (jsonData.isEmpty()) return listOf()
    return Json { }.decodeFromString(jsonData)
}

fun getNameMappingsFromFile(file: File?, forUsernames:Boolean): List<NameMapping> {
    if (file == null) return listOf()
    if (!file.exists()) return listOf()
    if (file.extension.toLowerCase() == "json") {
        val jsonData = Files.readString(file.toPath()).trim()
        if (jsonData.isEmpty()) return listOf()
        return Json { }.decodeFromString(jsonData)
    }

    if(file.extension.toLowerCase() == "csv") {
        val nameKey = if (forUsernames) OpenJdkPeopleCsvReader.NameKey.Username else OpenJdkPeopleCsvReader.NameKey.RealName
        return OpenJdkPeopleCsvReader.read(file, true, nameKey)
    }

    throw IllegalArgumentException("Unknown file extensions type for name mapping: $file")
}

fun <T> handleCsvOutput(
    programSharedData: ProgramSharedData,
    data: Collection<DataPoint<T>>,
    title: String,
    indexLabel: String
) {
    val lines = StringBuffer()
    if (data.isNotEmpty()) {
        lines.appendLine(data.first().toCsvHeader(indexLabel))
    }

    data.forEach {
        lines.appendLine(it.toCsvValue())
    }

    if (programSharedData.outputCsv) {
        Files.writeString(programSharedData.outputCsvFilename!!.toPath(), lines.toString())
    }

    if (programSharedData.outputCli) {
        TermUi.echo(title)
        TermUi.echo(lines)
    }
}

