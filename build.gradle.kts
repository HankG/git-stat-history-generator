import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.31"
    kotlin("plugin.serialization") version "1.4.31"
    id("com.github.johnrengelman.shadow") version "6.1.0"
    application
}

group = "me.hankg"
version = "1.0-SNAPSHOT"

val clicktVersion = "3.1.0"
val csvVersion = "0.15.1"
val kotlinxSerializationVersion = "1.1.0"
val resultMonadVersion = "1.1.11"

repositories {
    jcenter()
    mavenCentral()
}

dependencies {
    implementation("com.github.ajalt.clikt:clikt:$clicktVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinxSerializationVersion")
    implementation("com.michael-bull.kotlin-result:kotlin-result:$resultMonadVersion")
    implementation("com.github.doyaaaaaken:kotlin-csv-jvm:$csvVersion")
    testImplementation(kotlin("test-common"))
    testImplementation(kotlin("test-junit"))
    implementation(kotlin("stdlib-jdk8"))
}

tasks.test {
    useJUnit()
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}

application {
    mainClassName = "MainKt"
}
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "11"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "11"
}