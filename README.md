# Git Stat History Generator

Project for creating detailed statistical information about a project's Git history. There are two elements
* The main program which can generate a self contained uber jar
* A Python script that calls the main program to generate graphs based on the generated data

## Building

To build you'll need a JDK 11 or higher installed on your system. I use OpenJDK so this is what it was tested on. You'll
also need a git client if you are checking out the source directly rather than downloading a zip file of it.

To go from a clean checkout to a self contained Java Archive file:

```bash
git clone https://gitlab.com/HankG/git-stat-history-generator.git
cd git-stat-history-generator
./gradlew shadowJar
```
The executable will then be in the `./build/libs` folder for use.

## Usage

To use the program invoke from the command line using `java -jar` such as below where we use the `--help` flag to list
the help:

```bash
$ java -jar git-stat-history-generator-1.0-SNAPSHOT-all.jar 
Usage: git-stat-history [OPTIONS] COMMAND [ARGS]...

Options:
  --real-name-mappings PATH  Mapping file of real names to values (like
                             companies or common names)
  --user-name-mappings PATH  Mapping file of real names to values (like
                             companies or common names)
  --earliest-date TEXT       ISO formatted date/time
                             '2020-10-16T00:00:44+09:00' earliest date for
                             analysis
  --latest-date TEXT         ISO formatted date/time
                             '2020-10-16T00:00:44+09:00' earliest date for
                             analysis
  --output-csv PATH          Toggle output to the specified CSV file
  --output-cli               Whether to output text data to command line
  -h, --help                 Show this message and exit

Commands:
  load-archive             Load the archive of a previously processed
                           repository
  commit-frequency         Commit frequency data generation
  commit-summary-timeline  Timeline of raw commit summary data (no binning or
                           grouping)
  load-repo                Reading/Saving of a Git Repository
  timeline                 Timeline statistics
  unique-users             List of all unique author name/email
  unique-file-extensions   Determine the unique file extensions in a
                           repository
  user-summaries           Summary data for user's repository interactions
```
To get help on individual commands use the --help on that:

```bash
$ java -jar build/libs/git-stat-history-generator-1.0-SNAPSHOT-all.jar load-archive --help
Usage: git-stat-history load-archive [OPTIONS] archive-file

  Load the archive of a previously processed repository

Options:
  --ignore-merges  Ignore merges when generating results
  --ignore-tags    Ignore tags when generating results (tags can skew totals
                   by double counting commit summaries)
  -h, --help       Show this message and exit

Arguments:
  archive-file  Path to the archive from a previous run
```

Commands besides `load-archive` and `load-repo` are only accessible after an archive or repo 
are loaded since they are analyses on loaded data. There are several types of analysis one can choose from,
as shown in the command line help. The above settings listed as part of the main --help are 
the ones available for all commands. An example of a sub-command help command and output is:

```bash
$ java -jar build/libs/git-stat-history-generator-1.0-SNAPSHOT-all.jar load-repo /tmp/git-stat-history-generator/ timeline --help
Usage: git-stat-history timeline [OPTIONS] timezone

  Timeline statistics

Options:
  --stat-type [Activities|Commits|Files|Lines|LineVolume|TotalActivities|TotalCommits|TotalFiles|TotalLines]
                                   Type of timeline to generate (defaults to
                                   total lines)
  --time-units [Hours|Days|Weeks|Months]
                                   Time type for timeline (defaults to weeks)
  --users                          Data for users (default doesn't generate)
  --no-total                       Data for entire repository (default the
                                   total is generated)
  --top-users INT                  Only include top X users (default all
                                   users)
  --no-others-grouping             When using 'top users' groups remaining
                                   stats into one category (default does
                                   grouping)
  -h, --help                       Show this message and exit

Arguments:
  timezone  Timezone to shift to if specified such as UTC or EST or can use
            'local' system default
```

One needs to load a repository or a repository archive for processing. It is possible to run the analysis directly on a repository with the `load-repo` command or
to process a repository once into an archive file and then load that repository in using the 
`load-archive` command.

For example to output the user summaries for this repository to the command line
one can execute this command in the root:

```bash
java -jar build/libs/git-stat-history-generator-1.0-SNAPSHOT-all.jar --output-cli load-repo --branch-name main ./ user-summaries
```

If I wanted to store off the archive, so I could load it in another execution, that would look like:

```bash
java -jar build/libs/git-stat-history-generator-1.0-SNAPSHOT-all.jar load-repo --branch-name main --save /tmp/git-history.json ./
java -jar build/libs/git-stat-history-generator-1.0-SNAPSHOT-all.jar --output-cli load-archive /tmp/git-history.json user-summaries
```

For smaller repositories one can just load the repository directly. It should be noted though that the
`load-repo` command checks out the requested branch (or defaults to `main` if one doesn't specify). 
It is therefore best to not run this on a working copy since you may have changes which should be committed,
etc. 

Output is always a comma separated tabular format. One can choose to output directly to the command line
with the `output-cli` option and/or to a CSV file with the `output-csv` option, which takes a file 
argument as well. If no option is chosen then no output is generated.

Name mappings come in two flavors, the JSON data which reflects the internal document and a CSV format.
The name mappings files should be comma-separated CSV files with the relevant columns being:
* Column 0: The 'Real name' as it would appear in the commit information
* Column 1: The mapped name that should be substituted (often an organizational name)
* Column 2: The username for that committer

Name mappings are used in two different ways, to replace "real names" and to replace "user names".
"Real names" are the names used in the commit data to identify the user. It usually takes the form
of their real names, hence the moniker. "User names" are how people are identified in the text of a
commit message when they are tracking things like code reviews, authorizations, etc. Let's look at 
a `git log` entry from the OpenJDK project as an example:

```bash
Author: Daniel D. Daugherty <dcubed@openjdk.org>
Date:   Sat Apr 3 19:06:34 2021 +0000

    8264393: JDK-8258284 introduced dangling TLH race
    
    Reviewed-by: dholmes, rehn, eosterlund
```

In this case the entries for a CSV file with these people in it will look like:

```text
Full name, Organization, Username, Blog
Daniel D. Daugherty,Oracle,dcubed,
David Holmes,Oracle,dholmes,http://blogs.oracle.com/dholmes/
Robbin Ehn,Oracle,rehn,
Erik Osterlund,Linnaeus University,eosterlund,
```

If you have no organizational data then you don't need to worry about either one. Statistics will
be generated based on the actual values without mappings. If the project does not use username data
within their commit messages then it can be ommitted as well. Name mappings can also be used
to correct cases of name variations. For example on a project where my contributions are committed as both
`Hank G` and `Hank Grabowski` I could normalize that with a file like below even though I'm not tracking:

```bash
Commit Name, Common Name, Username
Hank G, Hank, hank 
Hank Grabowski,   Hank, hank 

```

It is also possible to have the file be a full JSON file that corresponds to the internal format.
The advantage this gives the user is that the mappings can change over time. This can be useful if
a contributor changes organizations over the life of a project. An example file is here:

```json
[
    {
        "originalFieldValue": "Terry Smith",
        "newFieldValues": [
            {
                "latestValidDate": "2011-01-01T00:00:00-04:00",
                "newValue": "Terry Smith"
            },
            {
                "latestValidDate": "2012-01-01T00:00:00-04:00",
                "newValue": "Company 1"
            },
            {
                "latestValidDate": "2015-01-01T00:00:00-04:00",
                "newValue": "Company 2"
            }
        ]
    },
    {
        "originalFieldValue": "Aiden Fisher",
        "newFieldValues": [
            {
                "latestValidDate": "2014-01-01T00:00:00-04:00",
                "newValue": "Company 3"
            },
            {
                "latestValidDate": "2011-01-01T00:00:00-04:00",
                "newValue": "Company 1"
            },
            {
                "latestValidDate": "2012-01-01T00:00:00-04:00",
                "newValue": "Company 2"
            }
        ]
    },
    {
        "originalFieldValue": "Pat Miller",
        "newFieldValues": [
            {
                "latestValidDate": "3010-01-01T00:00:00-04:00",
                "newValue": "Pat Miller"
            }
        ]
    }
]
```

# Generation Script

Along with the main tool there is a Python script for generating graphs from the data 
generated by the script. The tool requires Python 3.x. The requirements file can be used
to load up the necessary Python libraries to run:

```bash
pip3 install -r requirements.txt
```

The tool calls the main program to generate CSV data for processing on the fly based on a JSON
settings file. An example execution is:

```bash
python3 generate.py git-stat.jar projects/elastic_kibana/inputs/kibana_settings.json
```


The JSON file has three main elements:
* `archive_path`: Path to the archive file that is going to be processed (see `load-archive` option above)
* `graphs`: an array of graph settings for each graph that the user wants generated
* `versions`: an array of `versions` to break the project up into by date

An example configuration file is below:

```json
{
  "archive_path": "projects/elastic_kibana/outputs/elastic_search.json",
  "graphs": [
    {
      "name": "Total Lines Per Month",
      "filename_base": "total_lines_per_month",
      "data_type": "timeline",
      "plot_type": "area",
      "as_percent": false,
      "drop_last_column": false,
      "time_units": "Months",
      "stat_type": "Lines"
    },
    {
      "name": "Total Files Per Month",
      "filename_base": "total_files_per_month",
      "data_type": "timeline",
      "plot_type": "line",
      "as_percent": false,
      "drop_last_column": false,
      "time_units": "Months",
      "stat_type": "Files",
      "has_legend": false
    },
    {
      "name": "Project File Count",
      "filename_base": "file_count",
      "data_type": "timeline",
      "plot_type": "area",
      "as_percent": false,
      "drop_last_column": false,
      "time_units": "Months",
      "stat_type": "TotalFiles",
      "has_legend": false
    },
    {
      "name": "Project Line Count",
      "filename_base": "line_count",
      "data_type": "timeline",
      "plot_type": "area",
      "as_percent": false,
      "drop_last_column": false,
      "time_units": "Months",
      "stat_type": "TotalLines",
      "has_legend": false
    },
    {
      "name": "Total Commits Per Month",
      "filename_base": "total_commits_per_month",
      "data_type": "timeline",
      "plot_type": "area",
      "as_percent": false,
      "drop_last_column": false,
      "time_units": "Months",
      "stat_type": "Commits",
      "has_legend": false
    },
    {
      "name": "Total Line Volume Per Month",
      "filename_base": "total_linevolume_per_month",
      "data_type": "timeline",
      "plot_type": "area",
      "as_percent": false,
      "drop_last_column": false,
      "time_units": "Months",
      "stat_type": "LineVolume",
      "has_legend": false
    },
    {
      "name": "Top Contributing Organizations",
      "filename_base": "top_contrib_orgs",
      "mappings": "projects/elastic_kibana/inputs/ElasticKibanaUserMappings.csv",
      "data_type": "user-summaries",
      "plot_type": "pie",
      "max_users": 10,
      "column":"Commits",
      "as_percent": true,
      "drop_last_column": true
    },
    {
      "name": "Top Contributing Organizations LineVolume History",
      "mappings": "projects/elastic_kibana/inputs/ElasticKibanaUserMappings.csv",
      "filename_base": "top_contrib_orgs_linevolume_history",
      "data_type": "timeline",
      "plot_type": "area",
      "max_users": 10,
      "time_units": "Months",
      "stat_type": "LineVolume"
    },
    {
      "name": "Top Contributing Organizations % LineVolume History",
      "mappings": "projects/elastic_kibana/inputs/ElasticKibanaUserMappings.csv",
      "filename_base": "top_contrib_orgs_linevolume_history_pct",
      "data_type": "timeline",
      "plot_type": "area",
      "max_users": 10,
      "time_units": "Months",
      "as_percent": true,
      "stat_type": "LineVolume"
    },
    {
      "name": "Top Contributing Organizations Commit History",
      "mappings": "projects/elastic_kibana/inputs/ElasticKibanaUserMappings.csv",
      "filename_base": "top_contrib_orgs_commit_history",
      "data_type": "timeline",
      "plot_type": "area",
      "max_users": 10,
      "drop_last_column": true,
      "time_units": "Months",
      "stat_type": "Commits"
    },
    {
      "name": "Top Contributing Organizations % Commit History",
      "mappings": "projects/elastic_kibana/inputs/ElasticKibanaUserMappings.csv",
      "filename_base": "top_contrib_orgs_commit_history_pct",
      "data_type": "timeline",
      "plot_type": "area",
      "max_users": 10,
      "drop_last_column": true,
      "time_units": "Months",
      "as_percent": true,
      "stat_type": "Commits"
    },
    {
      "name": "Commits History By Users",
      "filename_base": "commits_history_by_users",
      "data_type": "timeline",
      "plot_type": "area",
      "time_units": "Months",
      "stat_type": "Commits",
      "drop_last_column": true,
      "max_users":10
    },
    {
      "name": "Commits History % By Users",
      "filename_base": "commits_history_by_users_pct",
      "data_type": "timeline",
      "plot_type": "area",
      "time_units": "Months",
      "stat_type": "Commits",
      "as_percent": true,
      "drop_last_column": true,
      "max_users":10
    },
    {
      "name": "Top LineVolume History By User",
      "filename_base": "top_linevolume_by_user",
      "data_type": "timeline",
      "plot_type": "area",
      "time_units": "Months",
      "stat_type": "LineVolume",
      "max_users":10
    },
    {
      "name": "Top % LineVolume History By User",
      "filename_base": "top_linevolume_by_user_pct",
      "data_type": "timeline",
      "plot_type": "area",
      "time_units": "Months",
      "stat_type": "LineVolume",
      "as_percent": true,
      "max_users":10
    }
  ],
  "versions": [
    {
      "label": "Project"
    },
    {
      "label": "1.0",
      "latest_date": "2014-02-12T16:18:13+00:00"
    },
    {
      "label": "2.0",
      "earliest_date": "2014-02-12T16:18:13+00:00",
      "latest_date": "2015-10-21T23:01:03+02:00"
    },
    {
      "label": "5.0",
      "earliest_date": "2015-10-21T23:01:03+02:00",
      "latest_date": "2016-10-25T23:08:59-04:00"
    },
    {
      "label": "6.0",
      "earliest_date": "2016-10-25T23:08:59-04:00",
      "latest_date": "2017-11-14T08:35:34-05:00"
    },
    {
      "label": "7.0",
      "earliest_date": "2017-11-14T08:35:34-05:00",
      "latest_date": "2019-04-10T11:41:52-04:00"
    },
    {
      "label": "7.x",
      "earliest_date": "2019-04-10T11:41:52-04:00"
    }
  ]
}
```