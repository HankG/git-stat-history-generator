from matplotlib import pyplot as plt
import datetime
import json
import matplotlib as mpl
import os.path
import pandas as pd
import subprocess
import sys


class Version:
    def __init__(self, label, earliest_date, latest_date):
        self.label = label
        self.earliest_date = earliest_date
        self.latest_date = latest_date

    def __str__(self):
        return f"Version[label={self.label}, from={self.earliest_date}, to={self.latest_date}]"


class Graph:
    def __init__(self, name, data_type, plot_type, filename_base, max_users=0, time_units=None, stat_type=None,
                 mappings_file=None, as_percent=False, column=None, drop_last_column=True, has_legend=True):
        self.name = name
        self.mappings_file = mappings_file
        self.data_type = data_type
        self.plot_type = plot_type
        self.filename_base = filename_base
        self.max_users = max_users
        self.time_units = time_units
        self.stat_type = stat_type
        self.as_percent=as_percent
        self.drop_last_column=drop_last_column
        self.column = column
        self.has_legend = has_legend

    def __str__(self):
        return f"Version[name={self.name}, mappings_file={self.mappings_file}, data_type={self.data_type}, " \
               f"plot_type={self.plot_type}, filename_base={self.filename_base}, max_users={self.max_users}, " \
               f"time={self.time_units}, stat={self.stat_type}, as_percent={self.as_percent}, " \
               f"drop_last_column={self.drop_last_column}, column={self.column}, has_legend={self.has_legend}]"


def pie_chart(filename: str, column: str, title: str, output_filename: str):
    df = pd.read_csv(filename, skipinitialspace=True, index_col=0)
    fig, ax = plt.subplots(figsize=(6, 4))
    sorted_values = df.sort_values(by=column, ascending=False)
    sorted_values.plot.pie(
        ax=ax,
        y=column,
        startangle=90,
        radius=1.1,
        labels=None,
        center=(0.5, 0.5),
        cmap = 'tab20c'
    )
    ax.set_ylabel('')
    ax.set_title(title)
    ax.legend(loc='center left', bbox_to_anchor=(0.95, 0.5), labels=sorted_values.index)
    fig.tight_layout()
    fig.savefig(output_filename)


def timeline(filename: str, as_area: bool, title: str, drop_last_column: bool, has_legend:bool,
             as_percent: bool, output_filename: str):
    df = pd.read_csv(filename, skipinitialspace=True, index_col=0, parse_dates=['Date'], dayfirst=False)
    fig, ax = plt.subplots(figsize=(10, 6))
    if drop_last_column:
        df.pop(df.columns[df.columns.size - 1])

    if as_percent:
        data = df.divide(df.sum(axis=1)/100.0, axis=0)
    else:
        data = df

    if as_area:
        data.plot.area(ax=ax, cmap='tab20c')
    else:
        data.plot.line(ax=ax, cmap='tab20c')

    if as_percent:
        ax.set_ylim([0, 100])
    ax.set_title(title)
    ax.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    if has_legend:
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    else:
        ax.get_legend().remove()
    fig.tight_layout()
    fig.savefig(output_filename)


def generate_csv(jar_path: str,  archive_path: str, output_file: str, version: Version,  graph_settings: Graph):
    args = [
        'java',
        '-jar', jar_path,
        '--output-csv', output_file
    ]

    if version.earliest_date is not None:
        args.append(f'--earliest-date={version.earliest_date}')

    if version.latest_date is not None:
        args.append(f'--latest-date={version.latest_date}')

    if graph_settings.mappings_file is not None:
        args.append(f'--real-name-mappings={graph_settings.mappings_file}')

    command = graph_settings.data_type
    command_args = []
    if graph_settings.max_users is not None:
        if command == "timeline":
            command_args.append('--users')
        command_args.append(f'--top-users={graph_settings.max_users}')

    if graph_settings.stat_type is not None:
        command_args.append(f'--stat-type={graph_settings.stat_type}')

    if graph_settings.time_units is not None:
        command_args.append(f'--time-units={graph_settings.time_units}')

    if command == "timeline":
        command_args.append('UTC')

    args = args + [
        'load-archive', archive_path,
        command
    ] + command_args
    print(" ".join(map(str,args)))
    subprocess.call(args)


def output_help():
    print("Usage: generate.py stat-gen-jar-file generate-py-config-file")


def extract_versions(settings):
    versions = []
    for v in settings["versions"]:
        label = v["label"]
        earliest_date = v.get("earliest_date", None)
        latest_date = v.get("latest_date", None)
        versions.append(Version(label, earliest_date, latest_date))
    return versions


def extract_graphs(settings):
    allowable_data_types = ["user-summaries", "timeline"]
    allowable_plot_types = ['pie', 'area', 'line']
    allowable_time_units = ["Hours", "Days", "Weeks", "Months"]
    allowable_stat_types = ["Activities", "Commits", "Files", "Lines", "LineVolume", "TotalActivities", "TotalCommits",
                            "TotalFiles", "TotalLines"]
    graphs = []
    for g in settings["graphs"]:
        name = g["name"]
        filename_base = g["filename_base"]
        mappings_file = g.get("mappings", None)
        max_users = g.get("max_users", None)
        data_type = g["data_type"]
        assert data_type in allowable_data_types
        plot_type = g["plot_type"]
        assert plot_type in allowable_plot_types
        time_units = None
        stat_type = None
        if data_type == "timeline":
            time_units = g["time_units"]
            assert time_units in allowable_time_units
            stat_type = g["stat_type"]
            assert stat_type in allowable_stat_types
        as_percent = g.get("as_percent", False)
        drop_last_column = g.get("drop_last_column", True)
        column = g.get("column", None)
        has_legend = g.get("has_legend", True)

        graphs.append(Graph(name=name, data_type=data_type, plot_type=plot_type, filename_base=filename_base,
                            time_units=time_units, stat_type=stat_type, max_users=max_users, column=column,
                            mappings_file=mappings_file, as_percent=as_percent, drop_last_column=drop_last_column,
                            has_legend=has_legend))
    return graphs


def main():
    print (sys.argv)
    if len(sys.argv) != 3:
        output_help()
        exit(-1)

    jar_path = sys.argv[1]
    if not os.path.isfile(jar_path):
        print(f"Jar file does not exist: {jar_path}")

    config_path = sys.argv[2]
    if not os.path.isfile(config_path):
        print(f"Config file does not exist: {config_path}")

    with open(config_path, "r") as read_file:
        settings = json.load(read_file)

    archive_path = settings["archive_path"]
    assert os.path.exists(archive_path)
    output_folder_name = "results_" + datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S")

    versions = extract_versions(settings)
    graphs = extract_graphs(settings)
    os.makedirs(output_folder_name)
    for v in versions:
        print(f"Generating graphs for {v.label}")
        version_folder = os.path.join(output_folder_name, v.label)
        os.makedirs(version_folder)
        for g in graphs:
            csv_file = os.path.join(version_folder, g.filename_base + ".csv")
            png_file = os.path.join(version_folder, g.filename_base + ".png")
            generate_csv(jar_path=jar_path, archive_path=archive_path, output_file=csv_file, version=v, graph_settings=g)
            print(f"Generated graph data {g.name} at: {csv_file}")
            if g.plot_type == 'pie':
                pie_chart(csv_file, g.column, g.name, png_file)
            elif g.plot_type == 'area' or g.plot_type == 'line':
                as_area = g.plot_type == 'area'
                timeline(filename=csv_file, as_area=as_area, title=g.name, drop_last_column=g.drop_last_column,
                         as_percent=g.as_percent, output_filename=png_file, has_legend=g.has_legend)
            print(f"Generated graph {g.name} at: {png_file}")
            print()

        print("------------------------------------------")
    exit(0)


if __name__ == "__main__":
    main()
